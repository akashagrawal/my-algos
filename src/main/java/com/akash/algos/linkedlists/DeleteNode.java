package com.akash.algos.linkedlists;

public class DeleteNode {
	
	/*
	 * If node given to be deleted is last node then that cannot be solved. May be you can mark
	 * the field as dummy in the node
	 */
	public static boolean deleteNode(Node node) {
		if(node == null || node.next == null) {
			return false;
		} else {
			node.value = node.next.value;
			node.next = node.next.next;
			return true;
		}
	}
}
