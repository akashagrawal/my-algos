package com.akash.algos.linkedlists;

public class DetectCircularList {
	public static boolean isCircular(Node head) {
		Node runner = head;
		while(runner != null && runner.next != null) {
			runner = runner.next.next;
			head = head.next;
			if(head == runner) {
				return true;
			}
		}
		return false;
	}
}
