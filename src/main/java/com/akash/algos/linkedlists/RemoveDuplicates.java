package com.akash.algos.linkedlists;

import java.util.HashSet;
import java.util.Set;

public class RemoveDuplicates {

	/*
	 * This uses extra memory "Set" to keep track of duplicates but gives
	 * performance of O(n)
	 */
	public static void removeDuplicateNodes(Node currentNode) {
		Set<Integer> set = new HashSet<Integer>();
		Node previous = null;
		while (currentNode != null) {
			if(set.contains(currentNode.value)) {
				previous.next = currentNode.next;
			} else {
				previous = currentNode;
				set.add(currentNode.value);
			}
			currentNode = currentNode.next;
		}
	}
	
	/*
	 * This does NOT uses extra memory and therefore gives performance of O(n*n)
	 */
	public static void removeDupNoExtraBuffer(Node current) {
		while(current != null) {
			Node runner = current;
			while(runner.next != null) {
				if(runner.next.value == current.value) {
					runner.next = runner.next.next;
				} else {
					runner = runner.next;
				}
			}
			current = current.next;
		}
	}
}
