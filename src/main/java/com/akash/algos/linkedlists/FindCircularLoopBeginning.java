package com.akash.algos.linkedlists;

public class FindCircularLoopBeginning {
	public static Node findBeginning(Node head) {
		Node fastRunner = head, slowRunner = head;
		while(fastRunner != null && fastRunner.next != null) {
			fastRunner = fastRunner.next.next;
			slowRunner = slowRunner.next;
			if(slowRunner == fastRunner)
				break;
		}
		if(fastRunner == null || fastRunner.next.next == null)
			return null;
		slowRunner = head;
		while(slowRunner != fastRunner) {
			slowRunner = slowRunner.next;
			fastRunner = fastRunner.next;
		}
		return slowRunner;
	}
}