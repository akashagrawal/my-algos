package com.akash.algos.linkedlists;

public class FindKthLastElement {
	
	/*
	 * Iterative way using runner technique 
	 */
	public static Node findKthLastElement(Node head, int k) {
		if(head == null || k < 1)
			return null;
		Node runner = head;
		int count = 1;
		while(runner != null && count < k) {
			runner = runner.next;
			count++;
		}
		if(runner==null)
			return null;
		while(runner.next != null) {
			runner = runner.next;
			head = head.next;
		}
		return head;
	}
	
	/*
	 * Recursive way using integer object reference
	 */
	public static Node findKthLastRecursive(Node head, int k, IntWrapper i) {
		if(head == null)
			return null;
		Node node = findKthLastRecursive(head.next, k, i);
		i.value = i.value + 1;
		if(i.value == k)
			return head;
		return node;
	}
	public static class IntWrapper {
		public int value = 0;
	}
	
	/*
	 * Recursive way using a static integer
	 */
	public static int count = 0;
	public static Node findKthLastRecursiveUsingStaticInt(Node head, int k) {
		if(head == null)
			return null;
		Node node = findKthLastRecursiveUsingStaticInt(head.next, k);
		count = count + 1;
		if(count == k)
			return head;
		return node;
	}
}

