package com.akash.algos.linkedlists;

public class AddTwoNumbers {
	public static Node addTwoNumbers(Node n1, Node n2) {
		Node current = null;
		Node result = null;
		int carry = 0;
		while(n1 != null || n2 != null) {
			int v1=0, v2=0;
			if(n1 != null)
				v1 = n1.value;
			if(n2 != null)
				v2 = n2.value;
			int v = v1 + v2 + carry;
			carry = v/10;
			Node r = new Node(null, v%10);
			if(current == null) {
				current = r;
				result = current;
			} else {
				current.next = r;
				current = current.next;
			}
			if(n1 != null)
				n1 = n1.next;
			if(n2 != null)
				n2 = n2.next;
		}
		if(carry > 0) {
			Node r = new Node(null, carry);
			current.next = r;
		}
		return result;
	}
	
	public static Node addTwoNumbersRecursively(Node n1, Node n2, int carry) {
		if(n1==null && n2==null && carry==0) {
			return null;
		}
		int v1=0,v2=0;
		if(n1 != null)
			v1 = n1.value;
		if(n2 != null)
			v2 = n2.value;
		int v = v1 + v2 + carry;
		carry = v/10;
		Node r = new Node(null, v%10);
		if(n1!=null) n1 = n1.next;
		if(n2!=null) n2 = n2.next;
		Node temp = addTwoNumbersRecursively(n1, n2, carry);
		r.next = temp;
		return r;
	}
}
