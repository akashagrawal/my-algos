package com.akash.algos.linkedlists;

import java.util.Stack;

public class PalindromeList {
	public static boolean isPalindrome(Node slow) {
		Stack<Integer> stack = new Stack<Integer>();
		Node fast = slow;
		while(fast !=null && fast.next != null) {
			stack.push(slow.value);
			slow = slow.next;
			fast = fast.next.next;
		}
		if(fast!=null)
			slow = slow.next;
		while(slow != null) {
			if(slow.value != stack.pop())
				return false;
			slow = slow.next;
		}
		return true;
	}
}
