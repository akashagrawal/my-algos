package com.akash.algos.linkedlists;

public class PartitionAroundGivenValue {

	/*
	 * It partitions the given linked list around x such that all the nodes with values
	 * less than x comes before. All the nodes with values equal or greater comes after
	 */
	public static Node partition(Node head, int x) {
		Node beforeHead = null;
		Node beforeTail = null;
		Node afterHead = null;
		Node afterTail = null;
		
		while(head != null) {
			Node target = head;
			head = head.next;
			target.next = null;
			if(target.value < x) {
				if(beforeHead==null) {
					beforeHead = target;
					beforeTail = beforeHead;
				} else {
					beforeTail.next = target;
					beforeTail = target;
				}
			} else {
				if(afterHead==null) {
					afterHead = target;
					afterTail = afterHead;
				} else {
					afterTail.next = target;
					afterTail = target;
				}
			}
		}
		if(beforeTail == null)
			return afterHead;
		// merge
		beforeTail.next = afterHead;
		return beforeHead;
	}
}
