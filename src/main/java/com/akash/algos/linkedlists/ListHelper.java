package com.akash.algos.linkedlists;

public class ListHelper {
	
	public static Node createSinglyLinkedList(int noOfNodes, int[] values) {
		if(values != null && values.length != noOfNodes) {
			throw new RuntimeException("No of nodes values given needs to be equal to no of nodes wanted");
		}
		int startValue = 20;		
		if(values != null)
			startValue = values[0];
		Node head = new Node(null, startValue);
		Node current = head;
		for(int index = 1; index < noOfNodes; index++) {
			if(values != null)
				startValue = values[index];
			else
				startValue = startValue + 10;
			Node node = new Node(null, startValue);
			current.next = node;
			current = node;
		}
		return head;
	}
	
	public static Node createSinglyLinkedList(int noOfNodes) {
		int startValue = 10;		
		Node head = new Node(null, startValue);
		Node current = head;
		for(int index = 1; index < noOfNodes; index++) {
			startValue = startValue + 10;
			Node node = new Node(null, startValue);
			current.next = node;
			current = node;
		}
		return head;
	}

	public static void printLinkedList(Node head) {
		if(head == null)
			return;
		while(head.next != null) {
			System.out.print(head.value + " => ");
			head = head.next;
		}
		System.out.println(head.value);
	}

	public static int getNumberOfNodes(Node head) {
		int count = 0;
		while(head != null) {
			head = head.next;
			count++;
		}
		return count;
	}
}
