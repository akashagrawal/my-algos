package com.akash.algos.linkedlists;

public class Node {
	public Node next = null;
	public int value;
	
	public Node(Node nextNode, int v) {
		this.value = v;
		this.next = nextNode;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(value + " => ");
		Node printNext = next;
		int count = 0;
		while(printNext != null && count < 20) {
			sb.append(printNext.value + " => ");
			printNext = printNext.next;
			count++;
		}
		sb.append("NULL");
		return sb.toString();
	}
}
