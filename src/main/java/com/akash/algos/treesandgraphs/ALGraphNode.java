package com.akash.algos.treesandgraphs;

/*
 * A linked list based Adjacency List Graph Node
 */
public class ALGraphNode {
	public int value;
	public int cost;
	public ALGraphNode next = null;
	
	public ALGraphNode(int v) {
		this.value = v;
		this.cost = 0;
	}
	public ALGraphNode(int v, int c) {
		this.value = v;
		this.cost = c;
	}

	public void add(int v, int c) {
		ALGraphNode current = this;
		while(current.next != null) {
			current = current.next;
		}
		ALGraphNode newNode = new ALGraphNode(v, c);
		current.next = newNode;
	}
}
