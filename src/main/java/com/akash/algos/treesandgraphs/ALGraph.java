package com.akash.algos.treesandgraphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

/*
 * A linked list based Adjacency List Graph implementation
 */
public class ALGraph {
	private int vertex;
	private int edges;
	private ALGraphNode[] adjList;
	private Map<Integer, String> vertexNames;

	public ALGraph(int nVertex) {
		this.vertex = nVertex;
		adjList = new ALGraphNode[nVertex];
		for(int index=0; index < this.vertex; index++) {
			adjList[index] = new ALGraphNode(index);
		}
		this.edges = 0;
		vertexNames = new HashMap<Integer, String>();
	}
	
	public int numVertex() {
		return vertex;
	}
	public int numEdges() {
		return edges;
	}
	public void setVertexName(int v, String name) {
		validateVertex(v);
		vertexNames.put(v, name);
	}
	public String getVertexName(int v) {
		validateVertex(v);
		return vertexNames.get(v);
	}
	private void validateVertex(int v) {
		if(v < 0 || v >= vertex)
			throw new RuntimeException("");
	}
	
	public ALGraphNode getVertex(int index) {
		return adjList[index];
	}
	public void addUndirectedEdge(int from, int to, int cost) {
		addDirectedEdge(from, to, cost);
		adjList[to].add(from, cost);
	}
	public void addDirectedEdge(int from, int to, int cost) {
		validateVertex(from);
		validateVertex(to);
		adjList[from].add(to, cost);
		edges++;
	}
	
	public List<ALGraphNode> neighbors(int v) {
		validateVertex(v);
		ALGraphNode thisNode = adjList[v];
		List<ALGraphNode> result = new ArrayList<ALGraphNode>();
		while(thisNode.next != null) {
			result.add(thisNode.next);
			thisNode = thisNode.next;
		}
		return result;
	}
	
	public ALGraphNode getUnvisitedNeighbor(int v, boolean[] visited) {
		validateVertex(v);
		ALGraphNode vNode = adjList[v];
		ALGraphNode nextNode = vNode.next;
		while(nextNode != null) {
			if(visited[nextNode.value])
				nextNode = nextNode.next;
			else
				break;
		}
		return nextNode;
	}
	
	public int degree(int v) {
		validateVertex(v);
		ALGraphNode thisNode = adjList[v];
		int count = 0;
		while(thisNode.next != null) {
			count++;
		}
		return count;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		String Newline = System.getProperty("line.separator");
		sb.append("Graph: V=" + this.vertex + " E=" + this.edges + Newline);
		for(int vertex = 0; vertex < adjList.length; vertex++) {
			ALGraphNode vertexNode = adjList[vertex];
			sb.append(vertexNames.get(vertexNode.value) + " => ");
			while(vertexNode.next != null) {
				vertexNode = vertexNode.next;
				sb.append(vertexNames.get(vertexNode.value) + " ");
			}
			sb.append(Newline);
		}
		return sb.toString();
	}
	
	public void dfsRecursiveTraversal(int node) {
		boolean[] visited = new boolean[this.vertex];
		dfsRecursiveUtil(node, visited);
		System.out.println();
	}
	public void dfsRecursiveUtil(int node, boolean[] visited) {
		// visit the node
		System.out.print(vertexNames.get(node) + " ");
		visited[node] = true;
		List<ALGraphNode> nayburs = neighbors(node);
		for(ALGraphNode n : nayburs) {
			if(!visited[n.value])
				dfsRecursiveUtil(n.value, visited);
		}
	}
	
	public void dfsIterativeTraversal(int node) {
		boolean[] visited = new boolean[this.vertex];
		
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(node);
		visited[node] = true;
		System.out.print(vertexNames.get(node) + " ");
		
		while(!stack.isEmpty()) {
			int v = stack.peek();
			ALGraphNode v1 = getUnvisitedNeighbor(v, visited);
			if(v1 != null) {
				visited[v1.value] = true;
				stack.push(v1.value);
				System.out.print(vertexNames.get(v1.value) + " ");
			} else {
				stack.pop();
			}
		}
		System.out.println();
	}
	
	public void bfsIterativeTraversal(int node) {
		boolean[] visited = new boolean[this.vertex];
		
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(node);
		visited[node] = true;
		System.out.print(vertexNames.get(node) + " ");
		
		while(!q.isEmpty()) {
			int v = q.poll();
			List<ALGraphNode> nayburs = neighbors(v);
			for(ALGraphNode n : nayburs) {
				if(!visited[n.value]) {
					q.add(n.value);
					visited[n.value] = true;
					System.out.print(vertexNames.get(n.value) + " ");
				}
			}
		}
		System.out.println();
	}
}