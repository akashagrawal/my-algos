package com.akash.algos.treesandgraphs;

public class BinarySearchTree {
	private BinaryNode root = null;
	public BinarySearchTree(BinaryNode r) {
		this.root = r;
	}
	public BinaryNode getRoot() {
		return root;
	}
	public void setRoot(BinaryNode root) {
		this.root = root;
	}
	
	public BinaryNode findIterative(int target) {
		BinaryNode head = root;
		while(head != null) {
			if(head.value == target)
				return head;
			else {
				if(target < head.value)
					head = head.left;
				else
					head = head.right;
			}
		}
		return null;
	}
	
	public BinaryNode findRecursive(BinaryNode head, int target) {
		if(head == null)
			return null;
		if(head.value == target)
			return head;
		if(target < head.value)
			return findRecursive(head.left, target);
		else
			return findRecursive(head.right, target);
	}
	
	public BinaryNode insert(int target) {
		if(root == null) {
			root = insert(null, target);
			return root;
		}
		return insert(root, target);
	}
	
	/*
	 * It returns a node which should be used as the new root. Old root may be replaced with
	 * this new root
	 */
	private BinaryNode insert(BinaryNode node, int target) {
		if(node == null) {
			BinaryNode n = new BinaryNode(target);
			return n;
		}
		if(target <= node.value) {
			node.left = insert(node.left, target);
		} else {
			node.right = insert(node.right, target);
		}
		return node;
	}
	
	public int size() {
		return size(root);
	}
	
	private int size(BinaryNode node) {
		if(node == null)
			return 0;
		return size(node.left) + size(node.right) + 1;
	}
	
	public int maxDepth() {
		return maxDepth(this.root, 0);
	}
	
	private int maxDepth(BinaryNode node, int depth) {
		if(node == null)
			return 0;
		depth++;
		int leftDepth = maxDepth(node.left, depth);
		int rightDepth = maxDepth(node.right, depth);
		int childrenDepth = Math.max(leftDepth, rightDepth);
		return Math.max(childrenDepth, depth);
	}
	
	public int minValue() {
		return minValueRecursive(this.root);
	}

	public static int minValueRecursive(BinaryNode node) {
		if(node == null)
			return Integer.MAX_VALUE;
		return Math.min(minValueRecursive(node.left), node.value);
	}
	
	public int maxValue() {
		return maxValueIterative(this.root);
	}
	
	public static int maxValueIterative(BinaryNode node) {
		if(node == null)
			return Integer.MIN_VALUE;
		BinaryNode current = node;
		while(current.right != null) {
			current = current.right;
		}
		return current.value;
	}
	
	public int height() {
		return height(this.root);
	}
	public static int height(BinaryNode node) {
		if(node == null)
			return 0;
		return Math.max(height(node.left), height(node.right)) + 1;
	}
}