package com.akash.algos.treesandgraphs;

import java.util.ArrayList;
import java.util.LinkedList;

public class CreateListsFromTree {
	/*
	 * It takes a binary tree (node) and returns an array list where each element in the array
	 * list is the head of a linked list
	 */
	public static ArrayList<LinkedList<BinaryNode>> createLinkedListsFromTree(BinaryNode node) {
		ArrayList<LinkedList<BinaryNode>> lists = new ArrayList<LinkedList<BinaryNode>>();
		createLinkedListFromTree(node, lists, 0);
		return lists;
	}
	
	/*
	 * It is a modified pre-order traversal algo. Instead of visiting the node we add the node to the correct list
	 */
	private static void createLinkedListFromTree(BinaryNode node, ArrayList<LinkedList<BinaryNode>> lists, int level) {
		if(node == null)
			return;
		// visit node
		LinkedList<BinaryNode> levelList = null;
		if(lists.size() == level) {
			levelList = new LinkedList<BinaryNode>();
			lists.add(levelList);
		} else {
			levelList = lists.get(level);
		}
		levelList.add(node);
		
		// visit left child
		createLinkedListFromTree(node.left, lists, level + 1);
		// visit right child
		createLinkedListFromTree(node.right, lists, level + 1);
	}
}