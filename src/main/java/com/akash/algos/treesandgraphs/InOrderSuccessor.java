package com.akash.algos.treesandgraphs;

public class InOrderSuccessor {
	public static BNodeWithParent inOrderSucc(BNodeWithParent node) {
		if(node.right != null) {
			// find min on the node.right
			return min(node.right);
		} else {
			// go up until the node is the left node. Once it is a left child, just return its parent
			BNodeWithParent q = node;
			BNodeWithParent x = q.parent;
			while(x != null && x.left != q) {
				q = x;
				x = x.parent;
			}
			return x;
		}
	}
	private static BNodeWithParent min(BNodeWithParent node) {
		if(node == null)
			return null;
		while(node.left != null) {
			node = node.left;
		}
		return node;
	}
}

class BNodeWithParent {
	public int value;
	public BNodeWithParent left = null;
	public BNodeWithParent right = null;
	public BNodeWithParent parent = null;
	public BNodeWithParent(int v) {
		this.value = v;
	}
}