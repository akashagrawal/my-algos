package com.akash.algos.treesandgraphs;

public class BinaryNode {
	public BinaryNode left = null;
	public BinaryNode right = null;
	public int value;
	public BinaryNode(int v) {
		this.value = v;
	}
	public BinaryNode(int v, BinaryNode left, BinaryNode right) {
		this.value = v;
		this.left = left;
		this.right = right;
	}
	@Override
	public String toString() {
		return "BinaryNode [value=" + value + "]";
	}
}