package com.akash.algos.treesandgraphs;

public class TreeHelper {

	public static BinarySearchTree buildTree(int[] values) {
		BinarySearchTree bst = new BinarySearchTree(null);
		if(values == null || values.length < 1)
			return bst;
		for(int index=0; index < values.length; index++) {
			bst.insert(values[index]);
		}
		return bst;
	}
}
