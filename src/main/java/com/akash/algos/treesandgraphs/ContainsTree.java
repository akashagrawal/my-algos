package com.akash.algos.treesandgraphs;

public class ContainsTree {
	public static boolean containsTree(BinaryNode largeTree, BinaryNode smallTree) {
		if(smallTree == null)
			return true;
		return subTree(largeTree, smallTree);
	}
	
	private static boolean subTree(BinaryNode t1, BinaryNode t2) {
		if(t1 == null)
			return false;
		// check at t1 and then recurse underneath
		if(BinaryTreeAlgorithms.sameTree(t1, t2))
			return true;
		return (subTree(t1.left, t2) || subTree(t1.right, t2));
	}
}
