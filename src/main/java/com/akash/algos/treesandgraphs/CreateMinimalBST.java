package com.akash.algos.treesandgraphs;

public class CreateMinimalBST {
	/*
	 * Given array is a sorted array in increasing order with unique elements
	 */
	public static BinaryNode createMinimalBST(int[] array) {
		if(array == null || array.length == 0)
			return null;
		return createMinimalBST(array, 0, array.length - 1);
	}
	private static BinaryNode createMinimalBST(int[] array, int start, int end) {
		if(end < start)
			return null;
		// Fill it up
		int mid = (start + end)/2;
		BinaryNode node = new BinaryNode(array[mid]);
		node.left = createMinimalBST(array, start, mid - 1);
		node.right = createMinimalBST(array, mid + 1, end);
		return node;
	}
}