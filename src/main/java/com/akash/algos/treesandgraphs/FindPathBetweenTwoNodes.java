package com.akash.algos.treesandgraphs;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class FindPathBetweenTwoNodes {
	
	public static boolean searchBFS(ALGraph g, ALGraphNode start, ALGraphNode target) {
		validateVertex(start.value, g.numVertex());
		validateVertex(target.value, g.numVertex());
		
		boolean[] visited = new boolean[g.numVertex()];
		Queue<ALGraphNode> q = new LinkedList<ALGraphNode>();
		q.add(start);
		visited[start.value] = true;
		
		while(!q.isEmpty()) {
			ALGraphNode current = q.poll();
			List<ALGraphNode> nayburs = g.neighbors(current.value);
			for(ALGraphNode n : nayburs) {
				if(n.value == target.value) {
					return true;
				} else {
					if(!visited[n.value]) {
						q.add(n);
						visited[n.value] = true;
					}
				}
			}
		}
		return false;
	}
	
	public static boolean searchDFS(ALGraph g, ALGraphNode start, ALGraphNode target) {
		validateVertex(start.value, g.numVertex());
		validateVertex(target.value, g.numVertex());
		
		boolean[] visited = new boolean[g.numVertex()];
		Stack<ALGraphNode> stack = new Stack<ALGraphNode>();
		stack.push(start);
		visited[start.value] = true;
		
		boolean found = false;
		while(!stack.isEmpty()) {
			ALGraphNode node = stack.peek();
			ALGraphNode naybur = g.getUnvisitedNeighbor(node.value, visited);
			if(naybur != null) {
				stack.push(naybur);
				visited[naybur.value] = true;
				if(naybur.value == target.value) {
					found = true;
					break;
				}
			} else {
				stack.pop();
			}
		}
		
		if(!stack.isEmpty()) {
			Stack<ALGraphNode> path = new Stack<ALGraphNode>();
			while(!stack.isEmpty()) {
				path.push(stack.pop());
			}
			while(!path.isEmpty()) {
				System.out.print(g.getVertexName(path.pop().value) + " ");
			}
		}
		System.out.println();
		return found;
	}
	
	private static void validateVertex(int v, int total) {
		if(v < 0 || v >= total)
			throw new RuntimeException("vertex invalid");
	}
}