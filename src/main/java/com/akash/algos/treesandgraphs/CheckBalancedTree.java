package com.akash.algos.treesandgraphs;

public class CheckBalancedTree {

	public static boolean isBalanced(BinaryNode node) {
		if(node == null)
			return true;
		int leftHeight = BinarySearchTree.height(node.left);
		int rightHeight = BinarySearchTree.height(node.right);
		if(Math.abs(rightHeight - leftHeight) > 1)
			return false;
		return isBalanced(node.left) && isBalanced(node.right);
	}
	
	public static boolean isBalancedEfficient(BinaryNode node) {
		if(node == null)
			return true;
		if(checkHeight(node) == -1)
			return false;
		else
			return true;
	}
	
	private static int checkHeight(BinaryNode node) {
		if(node == null)
			return 0;
		int leftHt = checkHeight(node.left);
		int rightHt = checkHeight(node.right);
		if(leftHt == -1 || rightHt == -1)
			return -1;
		if( (leftHt - rightHt) < -1 || (leftHt - rightHt) > 1)
			return -1;
		else
			return Math.max(leftHt, rightHt) + 1;
	}
}