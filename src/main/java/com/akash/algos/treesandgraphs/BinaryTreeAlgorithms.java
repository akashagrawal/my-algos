package com.akash.algos.treesandgraphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTreeAlgorithms {
	
	public static void inorderTraversal(BinaryNode root) {
		inorderTraversal(root, new ArrayList<Integer>());
	}
	public static void inorderTraversal(BinaryNode root, List<Integer> list) {
		if(root == null)
			return;
		inorderTraversal(root.left, list);
		visitNode(root);
		list.add(root.value);
		inorderTraversal(root.right, list);
	}
	
	public static void preorderTraversal(BinaryNode root) {
		preorderTraversal(root, new ArrayList<Integer>());
	}
	public static void preorderTraversal(BinaryNode root, List<Integer> list) {
		if(root == null)
			return;
		visitNode(root);
		list.add(root.value);
		preorderTraversal(root.left, list);
		preorderTraversal(root.right, list);
	}

	public static void postorderTraversal(BinaryNode root) {
		postorderTraversal(root, new ArrayList<Integer>());
	}
	public static void postorderTraversal(BinaryNode root, List<Integer> list) {
		if(root == null)
			return;
		postorderTraversal(root.left, list);
		postorderTraversal(root.right, list);
		visitNode(root);
		list.add(root.value);
	}
	
	public static void levelorderTraversal(BinaryNode root) {
		levelorderTraversal(root, new ArrayList<Integer>());
	}
	
	public static void levelorderTraversal(BinaryNode root, List<Integer> list) {
		if(root == null)
			return;
		Queue<BinaryNode> q = new LinkedList<BinaryNode>();
		q.add(root);
		while(!q.isEmpty()) {
			BinaryNode node = q.remove();
			if(node.left != null)
				q.add(node.left);
			if(node.right != null)
				q.add(node.right);
			visitNode(node);
			list.add(node.value);
		}
	}

	private static void visitNode(BinaryNode node) {
		System.out.print(node.value + " ");
	}

	/*
	 * Check if a tree has a path (root to leaf) whose sum is equal to given sum
	 */
	public static boolean hasPathSum(BinaryNode node, int sum) {
		return hasPathSum(node, 0, sum);
	}
	private static boolean hasPathSum(BinaryNode node, int levelSum, int sum) {
		if(node == null)
			return false;
		if(node.left == null && node.right == null) {
			if((levelSum + node.value) == sum)
				return true;
			else
				return false;
		}
		levelSum = levelSum + node.value;
		boolean leftResult = hasPathSum(node.left, levelSum , sum);
		if(leftResult)
			return true;
		boolean rightResult = hasPathSum(node.right, levelSum , sum);
		if(rightResult)
			return true;
		return false;
	}

	/*
	 * Print all paths (root to leaf) of given tree
	 */
	public static void printPaths(BinaryNode node) {
		Queue<Integer> list = new LinkedList<Integer>();
		printPaths(node, list);
	}
	private static void printPaths(BinaryNode node, Queue<Integer> list) {
		if(node == null)
			return;
		list.add(node.value);
		if(node.left == null && node.right == null) {
			System.out.println(list);
			list.remove();
			return;
		}
		printPaths(node.left, list);
		printPaths(node.right, list);
		list.remove();
	}
	
	/*
	 * Print all paths (root to leaf) of given tree whose sum equals to given sum
	 */
	public static void printPathsMatchesSum(BinaryNode node, int sum) {
		List<Integer> list = new ArrayList<Integer>();
		printPathsMatchesSum(node, list, sum);
	}
	private static void printPathsMatchesSum(BinaryNode node, List<Integer> list, int sum) {
		if(node == null)
			return;
		list.add(node.value);
		if(node.left == null && node.right == null) {
			if(listSum(list) == sum)
				System.out.println(list);
			list.remove(list.lastIndexOf(node.value));
			return;
		}
		printPathsMatchesSum(node.left, list, sum);
		printPathsMatchesSum(node.right, list, sum);
		list.remove(list.lastIndexOf(node.value));
	}
	private static int listSum(List<Integer> list) {
		int count = 0;
		for(int i : list)
			count = count + i;
		return count;
	}
	
	/*
	 * Modifes the given tree such that the new tree is mirror of given tree
	 */
	public static void mirror(BinaryNode node) {
		if(node == null)
			return;
		mirror(node.left);
		mirror(node.right);
		BinaryNode temp = node.left;
		node.left = node.right;
		node.right = temp;
	}
	
	/*
	 * Modifies the given tree such that make a copy of the node and put the node as a left child
	 */
	public static void doubleTree(BinaryNode node) {
		if(node == null)
			return;
		doubleTree(node.left);
		doubleTree(node.right);
		// make a copy
		BinaryNode copy = new BinaryNode(node.value);
		copy.left = node.left;
		node.left = copy;
	}
	
	/*
	 * Checks given two tree if they are structurally identical. They are made of nodes with same values
	 * arranged in the same way
	 */
	public static boolean sameTree(BinaryNode node1, BinaryNode node2) {
		if(node1 == null && node2 == null) {
			// both empty true
			return true;
		} else if(node1 != null && node2 != null) {
			// both non-empty
			return (node1.value == node2.value &&
					sameTree(node1.left, node2.left) &&
					sameTree(node1.right, node2.right));
		} else {
			// one empty one not
			return false;
		}
	}
	
	public static boolean isBinarySearchTree(BinarySearchTree bst) {
		return isBinarySearchTree(bst.getRoot());
	}
	
	public static boolean isBinarySearchTree(BinaryNode node) {
		if(node == null)
			return true;
		// check if node's left-node's max value is greater than node's value
		// check if node's right-node's min value is less than node's value
		if(BinarySearchTree.maxValueIterative(node.left) > node.value ||
				BinarySearchTree.minValueRecursive(node.right) <= node.value) {
			return false;
		}
		return (isBinarySearchTree(node.left) && isBinarySearchTree(node.right)); 
	}
	
	public static boolean isBinarySearchTree2(BinarySearchTree bst) {
		return isBinarySearchTree2(bst.getRoot(), Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	public static boolean isBinarySearchTree2(BinaryNode node, int min, int max) {
		if(node == null)
			return true;
		if(node.value < min || node.value > max)
			return false;
		boolean left = isBinarySearchTree2(node.left, min, node.value);
		if(!left)
			return false;
		boolean right = isBinarySearchTree2(node.right, node.value+1, max);
		return right;
	}
}
