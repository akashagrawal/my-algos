package com.akash.algos.treesandgraphs;

public class LowestCommonAncestor {
	
	/*
	 * Binary Search Tree Recursive algo
	 */
	public static BinaryNode lcaBSTRecursive(BinaryNode root, BinaryNode n1, BinaryNode n2) {
		if(root == null)
			return null;
		if(root.value < n1.value && root.value < n2.value) {
			return lcaBSTRecursive(root.right, n1, n2);
		} else if(root.value > n1.value && root.value > n2.value) {
			return lcaBSTRecursive(root.left, n1, n2);
		} else
			return root;
	}

	/*
	 * Binary Search Tree Iterative algo
	 */
	public static BinaryNode lcaBSTIterative(BinaryNode root, BinaryNode n1, BinaryNode n2) {
		while(root != null) {
			if(root.value < n1.value && root.value < n2.value) {
				root = root.right;
			} else if(root.value > n1.value && root.value > n2.value) {
				root = root.left;
			} else
				return root;
		}
		return null;
	}
	
	/*
	 * Binary Tree algo
	 */
	public static BinaryNode lcaBinaryTree(BinaryNode root, BinaryNode n1, BinaryNode n2) {
		if(n1 == null || n2 == null)
			return root;
		if(!doesItExist(root, n1) || !doesItExist(root, n2))
			return null;
		return lcaBinaryTreeHelper(root, n1, n2);
	}
	public static BinaryNode lcaBinaryTreeHelper(BinaryNode root, BinaryNode n1, BinaryNode n2) {
		if(root == null)
			return null;
		if(root.value == n1.value || root.value == n2.value)
			return root;
		
		boolean is_n1_on_left = doesItExist(root.left, n1);
		boolean is_n2_on_left = doesItExist(root.left, n2);
		
		if(is_n1_on_left != is_n2_on_left)
			return root;
		
		if(is_n1_on_left) {
			return lcaBinaryTreeHelper(root.left, n1, n2);
		} else {
			return lcaBinaryTreeHelper(root.right, n1, n2);
		}
	}
	public static boolean doesItExist(BinaryNode root, BinaryNode node) {
		if(root == null)
			return false;
		if(root.value == node.value)
			return true;
		return (doesItExist(root.left, node) || doesItExist(root.right, node));
	}
}