package com.akash.algos.treesandgraphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/*
 * An Adjacency Matrix Graph implementation
 */
public class AMGraph {
	private int vertex;
	private int edges;
	private int[][] adjMtrx;
	private Map<Integer, String> vertexNames;

	public AMGraph(int v) {
		this.vertex = v;
		this.edges = 0;
		adjMtrx = new int[v][v];
		vertexNames = new HashMap<Integer, String>();
	}

	public int numVertex() {
		return vertex;
	}
	public int numEdges() {
		return edges;
	}
	public void setVertexName(int v, String name) {
		validateVertex(v);
		vertexNames.put(v, name);
	}
	private void validateVertex(int v) {
		if(v < 0 || v >= vertex)
			throw new RuntimeException("");
	}

	public void addUndirectedEdge(int from, int to, int cost) {
		addDirectedEdge(from, to, cost);
		adjMtrx[to][from] = cost;
	}
	public void addDirectedEdge(int from, int to, int cost) {
		validateVertex(from);
		validateVertex(to);
		adjMtrx[from][to] = cost;
		edges++;
	}

	public List<Integer> neighbors(int v) {
		validateVertex(v);
		Set<Integer> set = new HashSet<Integer>();
		for(int index=0; index < this.vertex; index++) {
			if(adjMtrx[v][index] > 0)
				set.add(index);
		}
		/*
		// also add edges coming in (could be different for a directed graph)
		for(int index=0; index < this.vertex; index++) {
			if(adjMtrx[index][v] > 0)
				set.add(index);
		}
		*/
		List<Integer> result = new ArrayList<Integer>();
		result.addAll(set);
		return result;
	}

	public int getUnvisitedNeighbor(int v, boolean[] visited) {
		validateVertex(v);
		int[] nayburs = adjMtrx[v];
		for(int index=0; index<nayburs.length;index++) {
			if(nayburs[index] > 0 && !visited[index])
				return index;
		}
		return -1;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		String Newline = System.getProperty("line.separator");
		sb.append("Graph: V=" + this.vertex + " E=" + this.edges + Newline);
		for(int vertex = 0; vertex < this.vertex; vertex++) {
			sb.append(vertexNames.get(vertex) + " => ");
			for(int col=0; col<this.vertex; col++) {
				if(adjMtrx[vertex][col] > 0)
					sb.append(vertexNames.get(col) + " ");
			}
			sb.append(Newline);
		}
		return sb.toString();
	}
	
	public void dfsRecursiveTraversal(int node) {
		boolean[] visited = new boolean[this.vertex];
		dfsRecursiveUtil(node, visited);
		System.out.println();
	}
	private void dfsRecursiveUtil(int node, boolean[] visited) {
		System.out.print(vertexNames.get(node) + " ");
		visited[node] = true;
		List<Integer> nayburs = neighbors(node);
		for(int n : nayburs) {
			if(!visited[n])
				dfsRecursiveUtil(n, visited);
		}
	}
	
	public void dfsIterativeTraversal(int node) {
		boolean[] visited = new boolean[this.vertex];
		
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(node);
		visited[node] = true;
		System.out.print(vertexNames.get(node) + " ");
		
		while(!stack.isEmpty()) {
			int n = stack.peek();
			int naybur = getUnvisitedNeighbor(n, visited);
			if(naybur != -1) {
				stack.push(naybur);
				visited[naybur] = true;
				System.out.print(vertexNames.get(naybur) + " ");
			} else {
				stack.pop();
			}
		}
		System.out.println();
	}

}