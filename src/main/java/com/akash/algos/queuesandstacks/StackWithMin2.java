package com.akash.algos.queuesandstacks;

import java.util.Stack;

public class StackWithMin2 extends Stack<Integer>{
	private static final long serialVersionUID = 5077388661243652903L;
	Stack<Integer> minStack = new Stack<Integer>();
	public void push(int value) {
		if(value < min())
			minStack.push(value);
		super.push(value);
	}
	public Integer pop() {
		int result = super.pop();
		if(result==min())
			minStack.pop();
		return result;
	}
	public int min() {
		if(minStack.isEmpty())
			return Integer.MAX_VALUE;
		else
			return minStack.peek();
	}
}