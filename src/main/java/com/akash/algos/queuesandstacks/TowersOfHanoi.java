package com.akash.algos.queuesandstacks;

import java.util.Stack;

public class TowersOfHanoi {
	/*private StackWithName<Integer> source, destination, spare;
	public TowersOfHanoi() {
		source.name = "source";
		destination.name = "destination";
		spare.name = "spare";
	}*/
	public static void moveTower(int n, StackWithName<Integer> source, StackWithName<Integer> destination,
			StackWithName<Integer> spare) {
		System.out.println("moveTower(" + n + "," + source.name + "," + destination.name + "," + spare.name + ")" );
		if(n<=0)
			return;
		moveTower(n-1, source, spare, destination);
		moveDisk(source, destination);
		moveTower(n-1, spare, destination, source);
	}
	private static void moveDisk(Stack<Integer> source, Stack<Integer> destination) {
		destination.push(source.pop());
	}
}
class StackWithName<T> extends Stack<T> {
	private static final long serialVersionUID = 5596492234559905670L;
	public String name = "";
}
