package com.akash.algos.queuesandstacks;

import java.util.Stack;

public class MyQueue {
	private Stack<Integer> stackNewest;
	private Stack<Integer> stackOldest;
	public MyQueue() {
		stackNewest = new Stack<Integer>();
		stackOldest = new Stack<Integer>();
	}
	public void push(int item) {
		stackNewest.push(item);
	}
	
	public int pop() {
		// move element from stackNewest and then pop
		shiftStacks();
		return stackOldest.pop();
	}
	
	public int peek() {
		// move element from stackNewest and then peek
		shiftStacks();
		return stackOldest.peek();
	}
	
	private void shiftStacks() {
		if(stackOldest.isEmpty()) {
			while(!stackNewest.isEmpty()) {
				stackOldest.push(stackNewest.pop());
			}
		}
	}
	
	public boolean isEmpty() {
		if(stackNewest.isEmpty() && stackOldest.isEmpty())
			return true;
		return false;
	}
	
	public int size() {
		return stackNewest.size() + stackOldest.size();
	}
}
