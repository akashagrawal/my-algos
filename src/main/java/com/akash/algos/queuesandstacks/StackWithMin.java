package com.akash.algos.queuesandstacks;

import java.util.Stack;

public class StackWithMin extends Stack<EnhancedNode>{
	private static final long serialVersionUID = -1190287767645555556L;
	public void push(int value) {
		int min = value;
		if(min > min())
			min = min();
		super.push(new EnhancedNode(value, min));
	}
	public int min() {
		int min = Integer.MAX_VALUE;
		if(!this.empty()) {
			min = this.peek().min;
		}
		return min;
	}
}

class EnhancedNode {
	public int value;
	public int min;
	public EnhancedNode(int value, int min) {
		this.min = min;
		this.value = value;
	}
	public String toString() {
		return "EnhancedNode [value=" + value + ", min=" + min + "]";
	}
}