package com.akash.algos.queuesandstacks;

import java.util.Stack;

public class StackSorting {
	public static void sort(Stack<Integer> unsorted, Stack<Integer> sorted) {
		while(! unsorted.isEmpty()) {
			int item = unsorted.pop();
			while(!sorted.isEmpty() && sorted.peek() > item) {
				unsorted.push(sorted.pop());
			}
			sorted.push(item);
		}
	}
}
