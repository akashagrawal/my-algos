package com.akash.algos.queuesandstacks;

/**
 * @author AGRAWAK
 */
public class QueueBasedOnArray {

	private int capacity = 0;
	private int[] data;
	private int head=-1;
	private int tail=-1;
	
	public QueueBasedOnArray(int capacity) {
		this.capacity = capacity;
		data = new int[this.capacity];
	}
	
	public void push(int element) {
		if(((tail+1) % capacity) == head) {
			throw new RuntimeException("Queue is full");
		} else if(isEmpty()) {
			head++;
			tail++;
			data[tail] = element;
		} else {
			tail = (tail + 1) % capacity;
			data[tail] = element;
		}
		// printQueue("pushed " + element);
	}
	
	public int pop() {
		int result;
		if(isEmpty()) {
			throw new RuntimeException("Queue is empty");
		} else if(head == tail) {
			result = data[head];
			data[head] = 0; // nullify old data
			head = -1;
			tail = -1;
		} else {
			result = data[head];
			data[head] = 0; // nullify old data
			head = (head + 1) % capacity;
		}
		// printQueue("popped " + result);
		return result;
	}
	
	public boolean isEmpty() {
		return (head == -1 && tail == -1);
	}
	
	public int length() {
		if(isEmpty())
			return 0;
		if(tail < head) {
			return tail + capacity - head + 1;
		} else {
			return tail - head + 1;
		}
	}
	
	@SuppressWarnings("unused")
	private void printQueue(String s) {
		System.out.println("head="+ head +" tail="+ tail + " " + s);
		for(int index=0; index <= (capacity-1); index++)
			System.out.print(data[index] + " ");
		System.out.println();
	}
}
