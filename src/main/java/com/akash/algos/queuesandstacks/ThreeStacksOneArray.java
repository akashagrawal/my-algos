package com.akash.algos.queuesandstacks;

public class ThreeStacksOneArray {
	private int[] data;
	private int[] tail = {-99, 0, 0, 0};
	private int capacityEachStack;
	public ThreeStacksOneArray(int capacityEachStack) {
		data =  new int[capacityEachStack * 3];
		this.capacityEachStack = capacityEachStack;
		tail[1] = -1;
		tail[2] = capacityEachStack - 1;
		tail[3] = capacityEachStack*2 - 1;
	}
	private int maxIndex(int stackNum) {
		return (stackNum * capacityEachStack) - 1;
	}
	private int minIndex(int stackNum) {
		return (stackNum - 1) * capacityEachStack;
	}
	public void push(int stackNum, int value) {
		if(stackNum > 3 || stackNum < 1 || tail[stackNum] >= maxIndex(stackNum))
			throw new RuntimeException("Can't push element. Check stack num or stack is full");
		data[++tail[stackNum]] = value;
	}
	public int pop(int stackNum) {
		if(stackNum > 3 || stackNum < 1 || tail[stackNum] < minIndex(stackNum))
			throw new RuntimeException("Can't pop element. Check stack num or stack is empty");
		int result = data[tail[stackNum]];
		tail[stackNum]--;
		return result;
	}
	public int size(int stackNum) {
		return (tail[stackNum] + 1) - ((stackNum -1 ) * capacityEachStack);
	}
	public int peek(int stackNum) {
		if(stackNum > 3 || stackNum < 1 || tail[stackNum] <= minIndex(stackNum))
			throw new RuntimeException("Can't peek element. Check stack num or stack is empty");
		int result = data[tail[stackNum]];
		return result;
	}
	public boolean isEmpty(int stackNum) {
		if(stackNum > 3 || stackNum < 1)
			throw new RuntimeException("Can't check stack. Incorrect stack num");
		if(tail[stackNum] <= minIndex(stackNum))
			return true;
		return false;
	}
}