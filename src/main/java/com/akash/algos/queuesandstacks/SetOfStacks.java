package com.akash.algos.queuesandstacks;

import java.util.ArrayList;
import java.util.Stack;

public class SetOfStacks {
	ArrayList<Stack<Integer>> stacks = new ArrayList<Stack<Integer>>();
	int maxItemPerStack;
	public SetOfStacks(int maxItemPerStack) {
		this.maxItemPerStack = maxItemPerStack;
	}
	public void push(int v) {
		Stack<Integer> lastStack = getLastStack();
		if(lastStack==null) {
			Stack<Integer> stack = new Stack<Integer>();
			stack.push(v);
			stacks.add(stack);
		} else {
			if(lastStack.size() < maxItemPerStack)
				lastStack.push(v);
			else {
				Stack<Integer> stack = new Stack<Integer>();
				stack.push(v);
				stacks.add(stack);
			}
		}
	}
	public int pop() {
		Stack<Integer> lastStack = getLastStack();
		if(lastStack==null) {
			throw new RuntimeException("Stack empty");
		} else {
			int v = lastStack.pop();
			if(lastStack.isEmpty())
				stacks.remove(stacks.size()-1);
			return v;
		}
	}
	private Stack<Integer> getLastStack() {
		if(stacks.size()==0)
			return null;
		else
			return stacks.get(stacks.size()-1);
	}
	public int size() {
		return stacks.size();
	}
}
