package com.akash.algos.queuesandstacks;

import java.util.LinkedList;

public class AnimalShelterQueue {
	private LinkedList<Dog> dogs = new LinkedList<Dog>();
	private LinkedList<Cat> cats = new LinkedList<Cat>();
	
	public void enqueue(Animal animal) {
		if(animal == null)
			throw new RuntimeException("Can't be null");
		if(animal instanceof Dog)
			dogs.push((Dog)animal);
		if(animal instanceof Cat)
			cats.push((Cat)animal);
	}
	public Animal dequeueAny() {
		if(dogs.isEmpty())
			return cats.poll();
		else if(cats.isEmpty())
			return dogs.poll();
		
		if(dogs.peek().getInDate() < cats.peek().getInDate())
			return dogs.poll();
		else
			return cats.poll();
	}
	public Dog dequeueDog() {
		return dogs.poll();
	}
	public Cat  dequeueCat() {
		return cats.poll();
	}
}

abstract class Animal {
	protected long inDate;
	public long getInDate() {
		return inDate;
	}
}

class Dog extends Animal {
	public Dog() {
		this.inDate = System.currentTimeMillis();
	}
}
class Cat extends Animal {
	public Cat(long date) {
		this.inDate = System.currentTimeMillis();
	}
}