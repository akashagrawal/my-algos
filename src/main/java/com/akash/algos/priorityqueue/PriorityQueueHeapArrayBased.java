package com.akash.algos.priorityqueue;

/*
 * This is heap based priority queue. Heaps are binary tree. We are 
 * using array for this heap (binary tree) implementation.
 */
public class PriorityQueueHeapArrayBased {
	private int size;
	private int numberOfElements;
	private PriorityData2[] data;
	
	public PriorityQueueHeapArrayBased(int size) {
		this.size = size;
		data = new PriorityData2[size];
		numberOfElements = 0;
	}
	public boolean isEmpty() {
		return (numberOfElements==0);
	}
	public int size() {
		return this.size;
	}
	public void insert(int item, int priority) {
		PriorityData2 pd = new PriorityData2(item, priority);
		// resizing array
		if(this.size <= numberOfElements) {
			resize();
		}
		data[numberOfElements] = pd;
		trickleUp(numberOfElements);
		numberOfElements++;
	}
	private void trickleUp(int position) {
		int parent =  (position - 1) / 2;
		PriorityData2 temp = data[position];
		while(position > 0 && data[parent].priority < temp.priority) {
			data[position] = data[parent];
			position = parent;
			parent = (position -1) / 2;
		}
		data[position] = temp;
	}
	// Doubles the array size and copies elements to new array
	private void resize() {
		this.size = this.size * 2;
		PriorityData2[] old = data;
		data = new PriorityData2[this.size];
		// copy
		for(int idx=0; idx<numberOfElements; idx++) {
			data[idx] = old[idx];
		}
	}

	public PriorityData2 remove() {
		PriorityData2 result = data[0];
		numberOfElements--;
		if(numberOfElements > 0 ) {
			data[0] = data[numberOfElements];
			trickleDown(0);
		}
		return result;
	}
	private void trickleDown(int position) {
		int child;
		int leftChildPosition = (2 * position) + 1;
		int rightChildPosition = (2 * position) + 2;
		if(leftChildPosition >= numberOfElements) {
			// means the element at position does not have children. it is a leaf
			// nothing to be done. lets initialize child to the position and it will end
			child = position;
		} else if(rightChildPosition == numberOfElements) {
			// means element does not have right child. It only has left child
			child = leftChildPosition;
		} // at this point element has both left and right child. determine larger among child
		else if(data[leftChildPosition].priority > data[rightChildPosition].priority) {
			child = leftChildPosition;
		} else {
			child = rightChildPosition;
		}
		// Now that we know larger child, determine if element is larger than child
		// if element is larger than nothing to be done else we swap child with the element
		if(data[position].priority < data[child].priority) {
			PriorityData2 temp = data[position];
			data[position] = data[child];
			data[child] = temp;
			trickleDown(child);
		}
	}
	public void print() {
		for(int idx=0; idx<numberOfElements; idx++) {
			System.out.print(data[idx] + " ");
		}
		System.out.println();
	}
}
class PriorityData2 {
	public int priority;
	public int item;
	public PriorityData2(int data, int priority) {
		this.item = data;
		this.priority = priority;
	}
	@Override
	public String toString() {
		return "[priority=" + priority + ", data=" + item + "]";
	}
}