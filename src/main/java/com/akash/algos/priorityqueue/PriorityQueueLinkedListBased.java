package com.akash.algos.priorityqueue;

/*
 * A linked list based priority queue implementation. A data value and a priority is supplied when inserting. A priority
 * higher in value represents higher priority. Highest priority data item is at the head of the list.
 */
public class PriorityQueueLinkedListBased {
	private int numberOfElements;
	PriorityNode data;
	public PriorityQueueLinkedListBased() {
		data = null;
		numberOfElements = 0;
	}
	public boolean isEmpty() {
		return (numberOfElements==0);
	}
	public int size() {
		return numberOfElements;
	}
	
	public void insert(int item, int priority) {
		PriorityNode node = new PriorityNode(item, priority);
		if(data == null || data.priorityValue < priority) {
			node.next = data;
			data = node;
			numberOfElements++;
			// print();
			return;
		}
		
		PriorityNode prev = data;
		PriorityNode current = data.next;
		while(current != null && current.priorityValue >= priority) {
			prev = current;
			current = current.next;
		}
		if(current == null) {
			prev.next = node;
			node.next = null;
		} else {
			node.next = current;
			prev.next = node;
		}
		numberOfElements++;
		// print();
	}
	
	public PriorityNode remove() {
		PriorityNode result = data;
		data = data.next;
		numberOfElements--;
		// print();
		return result;
	}
	
	public void print() {
		PriorityNode current = data;
		while(current != null) {
			System.out.print(current + " ");
			current = current.next;
		}
		System.out.println();
	}
}

class PriorityNode {
	int item;
	int priorityValue;
	PriorityNode next;
	public PriorityNode(int item, int priority) {
		this.item = item;
		this.priorityValue = priority;
		this.next = null;
	}
	@Override
	public String toString() {
		return "[data=" + item + ", priority=" + priorityValue + "]";
	}
}