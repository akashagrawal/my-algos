package com.akash.algos.priorityqueue;

/*
 * Priority of an item is based on a priority value given to that item. Higher the value higher the priority.
 * In this implementation array is sorted from low to high priority value and therefore the last item in the
 * array is of highest priority
 */
public class PriorityQueueArrayBased {
	private int numberOfElements;
	private PriorityData[] data;
	private int size;
	
	public PriorityQueueArrayBased(int size) {
		this.size = size;
		data = new PriorityData[size];
		numberOfElements = 0;
	}
	
	public boolean isEmpty() {
		return (numberOfElements == 0);
	}
	
	public int size() {
		return this.size;
	}
	
	public void insert(int item, int priorityValue) {
		// resizing array
		if(this.size <= numberOfElements) {
			resize();
		}
		PriorityData pd = new PriorityData(item, priorityValue);
		int index = numberOfElements - 1;
		while(index >= 0 && data[index].priorityValue > priorityValue) {
			data[index+1] = data[index];
			index--;
		}
		data[index+1] = pd;
		numberOfElements++;
	}
	
	// Doubles the array size and copies elements to new array
	private void resize() {
		this.size = this.size * 2;
		PriorityData[] old = data;
		data = new PriorityData[this.size];
		// copy
		for(int idx=0; idx<numberOfElements; idx++) {
			data[idx] = old[idx];
		}
	}
	
	public PriorityData remove() {
		if(isEmpty())
			return null;
		PriorityData result = data[numberOfElements];
		numberOfElements--;
		return result;
	}
	
	public void print() {
		for(int idx=0; idx<numberOfElements; idx++) {
			System.out.print(data[idx] + " ");
		}
		System.out.println();
	}
}

class PriorityData {
	int item;
	int priorityValue;
	public PriorityData(int item, int value) {
		this.item = item;
		this.priorityValue = value;
	}
	@Override
	public String toString() {
		return "[data=" + item + ", priority=" + priorityValue + "]";
	}
}