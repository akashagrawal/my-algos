package com.akash.algos.priorityqueue;

/*
 * It is a binary tree based heap implementation of Priority Queue. Max stays on top.
 */
public class PriorityQueueHeapBTreeBased {
	PriorityTreeNode root;
	public PriorityQueueHeapBTreeBased() {
		root = null;
	}
	public boolean isEmpty() {
		return (root==null);
	}
	public void insert(int item, int priority) {
		PriorityTreeNode nd = new PriorityTreeNode(item, priority);
		if(root == null)
			root = nd;
		else
			root = merge(nd, root);
	}
	public PriorityTreeNode remove() {
		if(root==null)
			return null;
		PriorityTreeNode result = root;
		if(root.left == null)
			root = root.right;
		else if(root.right == null)
			root = root.left;
		else
			root = merge(root.left, root.right);
		return result;
	}
	private PriorityTreeNode merge(PriorityTreeNode leftHeap, PriorityTreeNode rightHeap) {
		// Make left heap higher priority node. This allows us to move left node as the new root
		if(leftHeap.priority < rightHeap.priority) {
			PriorityTreeNode temp = leftHeap;
			leftHeap = rightHeap;
			rightHeap = temp;
		}
		if(leftHeap.right == null) {
			leftHeap.right = rightHeap;
		} else if(leftHeap.left == null) {
			leftHeap.left = rightHeap;
		} else if(leftHeap.left.priority > leftHeap.right.priority) {
			leftHeap.left = merge(leftHeap.left, rightHeap);
		} else {
			leftHeap.right = merge(leftHeap.right, rightHeap);
		}
		return leftHeap;
	}
}
class PriorityTreeNode {
	int item;
	int priority;
	PriorityTreeNode left;
	PriorityTreeNode right;
	public PriorityTreeNode(int item, int priority) {
		this.item = item;
		this.priority = priority;
		this.left = null;
		this.right = null;
	}
	@Override
	public String toString() {
		return "[data=" + item + ", priority=" + priority + "]";
	}
}