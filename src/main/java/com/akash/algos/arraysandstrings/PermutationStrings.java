package com.akash.algos.arraysandstrings;

public class PermutationStrings {
	
	private static int[] givenString;
	
	public static boolean arePermutationStrings(String str1, String str2) {
		if(str1 == null || str2 == null || str1.length() != str2.length())
			return false;
		givenString = new int[256];
		for(int index=0; index < str1.length(); index++) {
			int value = str1.charAt(index);
			givenString[value]++;
		}
		for(int index=0; index < str2.length(); index++) {
			int value = str2.charAt(index);
			givenString[value]--;
			if(givenString[index] > 0) {
				return false;
			}
		}
		return true;
	}
}
