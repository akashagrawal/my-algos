package com.akash.algos.arraysandstrings;

public class MatrixSetZeros {
	
	public static void setZeros(int[][] m) {
		boolean[] rows = new boolean[m.length];
		boolean[] cols = new boolean[m[0].length];
		
		for(int row=0; row < m.length; row++) {
			for(int col=0; col < m[0].length; col++) {
				if(m[row][col]==0) {
					rows[row] = true;
					cols[col] = true;
				}
			}
		}

		for(int row=0; row < m.length; row++) {
			for(int col=0; col < m[0].length; col++) {
				if(rows[row] || cols[col]) { // check for either rows row OR cols col has 0
					m[row][col]=0;
				}
			}
		}
	}

	public static void printMatrix(int[][] matrix) {
		for(int row = 0; row < matrix.length; row ++) {
			for(int col =0; col < matrix[row].length; col ++) {
				System.out.print(matrix[row][col] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
}
