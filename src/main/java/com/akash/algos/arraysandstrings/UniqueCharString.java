package com.akash.algos.arraysandstrings;

public class UniqueCharString {
	
	private static int[] givenString;
	
	public static boolean isUniqueCharInString(String str) {
		givenString = new int[256];
		if(str.length() > 256)
			return false;
		for(int index=0; index < str.length(); index++) {
			int value = str.charAt(index);
			givenString[value]++;
			if(givenString[value] > 1) {
				return false;
			}
		}
		return true;
	}
}
