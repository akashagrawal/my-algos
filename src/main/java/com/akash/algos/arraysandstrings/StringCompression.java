package com.akash.algos.arraysandstrings;

public class StringCompression {

	/*
	 * This method takes a string and any repeat characters are replaced by count
	 * e.g. aabcccccddd will become a2b1c5d3. If the resulting string is longer than
	 * the original, it returns the original
	 */
	public static String compressString(String str) {
		char last = str.charAt(0);
		int count = 1;
		StringBuilder sb = new StringBuilder();
		for(int index=1; index < str.length(); index++) {
			if(str.charAt(index) == last) {
				count++;
			} else {
				sb.append(last).append(count);
				last = str.charAt(index);
				count = 1;
			}
		}
		sb.append(last).append(count);
		if(sb.length() > str.length())
			return str;
		return sb.toString();
	}
}
