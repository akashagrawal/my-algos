package com.akash.algos.arraysandstrings;

public class RotateMatrix90 {
	
	/*
	 * This algorithm rotates matrix in place without using any space
	 */
	public static void rotateMatrix(int[][] m, int n) {
		for(int layer = 0; layer < n/2; layer++) {
			
			int start = layer;
			int end = n - 1 - layer;
			for(int index = start; index < end; ++index) {
				
				int offset = index - start;
				
				// save top in a temporary variable
				int temp = m[start][index];
				
				// shift left to top
				m[start][index] = m[end-offset][start];
				
				// shift bottom to left
				m[end-offset][start] = m[end][end-offset];
				
				// shift right to bottom
				m[end][end-offset] = m[index][end];
				
				// shift temp to top right
				m[index][end] = temp;
			}
		}
	}
	
	public static void rotateMatrixUsingExtraArray(int[][] m, int n) {
		int[] temp = new int[m.length];
		for(int layer = 0; layer < n/2; layer++) {
			int start = layer;
			int end = n - 1 - layer;
			
			for(int index = start; index <= end; index++) {
				// save top row is temp array
				temp[index] = m[start][index];
			}
			for(int index = start; index <= end; index++) {
				// save left row to top
				m[start][end - index] = m[index][start];
			}
			for(int index = start; index <= end; index++) {
				// save bottom row to left
				m[start][end - index] = m[end - start][index];
			}
		}
	}

	public static void printMatrix(int[][] matrix) {
		for(int row = 0; row < matrix.length; row ++) {
			for(int col =0; col < matrix[row].length; col ++) {
				System.out.print(matrix[row][col] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

}
