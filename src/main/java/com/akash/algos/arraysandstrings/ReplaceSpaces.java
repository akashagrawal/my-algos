package com.akash.algos.arraysandstrings;

public class ReplaceSpaces {
	
	/*
	 * This takes a string array with true length, and replace in place
	 */
	public static String replaceSpacesWithArrayPassed(char[] str, int length) {
		int count=0;
		for(int index=0; index < length; index++) {
			if(str[index] == ' ')
				count++;
		}
		int newLength = length + count * 2;
		int newCharLength = newLength;
		str[newLength] = '\0';
		for(int index=length-1; index >=0; index--) {
			if(str[index]==' ') {
				str[newLength-1] = '0';
				str[newLength-2] = '2';
				str[newLength-3] = '%';
				newLength = newLength - 3;
			} else {
				str[newLength-1] = str[index];
				newLength--;
			}
		}
		return String.valueOf(str, 0, newCharLength);
	}

	/*
	 * This takes a string, creates a new array and copies to the array
	 */
	public static String replaceSpacesWithStringPassed(String givenString) {
		int length = givenString.length();
		int count=0;
		for(int index=0; index < length; index++) {
			if(givenString.charAt(index) == ' ')
				count++;
		}
		int newLength = length + count * 2;
		char[] str = new char[newLength];
		for(int index=length-1; index >=0; index--) {
			if(givenString.charAt(index) == ' ') {
				str[newLength-1] = '0';
				str[newLength-2] = '2';
				str[newLength-3] = '%';
				newLength = newLength - 3;
			} else {
				str[newLength-1] = givenString.charAt(index);
				newLength--;
			}
		}
		return String.valueOf(str);
	}
}
