package com.akash.algos.priorityqueue;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class PriorityQueueHeapBTreeBasedTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testPriorityQueue() {
		PriorityQueueHeapBTreeBased pq = new PriorityQueueHeapBTreeBased();
		
		assertTrue(pq.isEmpty());
		
		pq.insert(107, 4);
		pq.insert(131, 3);
		pq.insert(144, 5);
		pq.insert(185, 4);
		PriorityTreeNode top = pq.remove();
		assertTrue(top.item==144);
		pq.insert(177, 5);
		pq.insert(120, 1);
		
		pq.insert(147, 2);
		
		assertTrue(pq.remove().item==177);
		assertTrue(pq.remove().item==185);
		assertTrue(pq.remove().item==107);
		assertTrue(pq.remove().item==131);
		assertTrue(pq.remove().item==147);
		assertTrue(pq.remove().item==120);
		assertTrue(pq.isEmpty());
	}
}