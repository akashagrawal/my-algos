package com.akash.algos.priorityqueue;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class PriorityQueueHeapArrayBasedTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testPriorityQueue() {
		PriorityQueueHeapArrayBased pq = new PriorityQueueHeapArrayBased(5);
		
		assertTrue(pq.isEmpty());
		assertTrue(pq.size()==5);
		
		pq.insert(107, 4);
		pq.insert(131, 3);
		pq.insert(144, 5);
		pq.insert(185, 4);
		PriorityData2 top = pq.remove();
		assertTrue(top.item==144);
		pq.insert(177, 5);
		pq.insert(120, 1);
		assertTrue(pq.size()==5);
		pq.print();
		
		pq.insert(147, 2);
		assertTrue(pq.size()==10);
		pq.print();
		
		assertTrue(pq.remove().item==177);
		assertTrue(pq.remove().item==107);
		assertTrue(pq.remove().item==185);
		assertTrue(pq.remove().item==131);
		assertTrue(pq.remove().item==147);
		assertTrue(pq.remove().item==120);
		assertTrue(pq.isEmpty());
	}
}