package com.akash.algos.priorityqueue;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class PriorityQueueArrayBasedTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testPriorityQueue() {
		PriorityQueueArrayBased pq = new PriorityQueueArrayBased(5);
		
		assertTrue(pq.isEmpty());
		assertTrue(pq.size()==5);
		
		pq.insert(107, 4);
		pq.insert(131, 3);
		pq.insert(144, 5);
		pq.insert(185, 4);
		pq.remove();
		pq.insert(177, 5);
		pq.insert(120, 1);
		assertTrue(pq.size()==5);
		pq.print();
		
		pq.insert(147, 2);
		assertTrue(pq.size()==10);
		pq.print();
	}
}