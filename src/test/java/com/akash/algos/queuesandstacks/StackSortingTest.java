package com.akash.algos.queuesandstacks;

import java.util.Stack;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class StackSortingTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void sortStack() {
		Stack<Integer> unsorted = new Stack<Integer>();
		unsorted.push(5);
		unsorted.push(10);
		unsorted.push(15);
		unsorted.push(20);
		unsorted.push(25);
		Stack<Integer> sorted = new Stack<Integer>();
		System.out.println(unsorted);
		StackSorting.sort(unsorted, sorted);
		System.out.println(sorted);
		Assert.assertEquals(5, sorted.size());
		Assert.assertEquals(0, unsorted.size());
		Assert.assertEquals(25, (int)sorted.peek());
	}

	@Test
	public void sortStack2() {
		Stack<Integer> unsorted = new Stack<Integer>();
		Stack<Integer> sorted = new Stack<Integer>();

		unsorted.push(7);
		unsorted.push(10);
		unsorted.push(5);
		
		sorted.push(1);
		sorted.push(3);
		sorted.push(8);
		sorted.push(12);
		
		System.out.println(unsorted);
		StackSorting.sort(unsorted, sorted);
		System.out.println(sorted);
		
		Assert.assertEquals(7, sorted.size());
		Assert.assertEquals(0, unsorted.size());
		Assert.assertEquals(12, (int)sorted.peek());
	}
}
