package com.akash.algos.queuesandstacks;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class SetOfStacksTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testStack() {
		SetOfStacks stacks = new SetOfStacks(3);
		stacks.push(5);
		stacks.push(7);
		stacks.push(9);
		stacks.push(11);
		Assert.assertEquals(2, stacks.size());
		Assert.assertEquals(11, stacks.pop());
		Assert.assertEquals(1, stacks.size());
	}
}
