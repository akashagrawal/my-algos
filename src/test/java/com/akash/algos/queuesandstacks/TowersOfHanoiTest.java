package com.akash.algos.queuesandstacks;

import org.junit.BeforeClass;
import org.junit.Test;

public class TowersOfHanoiTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testTowersOfHanoi() {
		StackWithName<Integer> source, destination, spare;
		source = new StackWithName<Integer>();
		source.name = "source";
		destination = new StackWithName<Integer>();
		destination.name = "destination";
		spare = new StackWithName<Integer>();
		spare.name = "spare";
		
		// fill up source
		source.push(5);
		source.push(4);
		source.push(3);
		source.push(2);
		source.push(1);
		
		System.out.println(source);
		TowersOfHanoi.moveTower(5, source, destination, spare);
		System.out.println(destination);
	}
}
