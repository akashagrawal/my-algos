package com.akash.algos.queuesandstacks;

import org.junit.BeforeClass;
import org.junit.Test;

import com.akash.algos.queuesandstacks.QueueBasedOnArray;

import static org.junit.Assert.assertEquals;

public class QueueBasedOnArrayTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void checkQueueLength() {
		QueueBasedOnArray q = new QueueBasedOnArray(4);
		assertEquals("", 0, q.length());
		
		q.push(5);
		assertEquals("", 1, q.length());
		
		q.pop();
		assertEquals("", 0, q.length());
	}

	@Test
	public void checkQueueCapacity() {
		QueueBasedOnArray q = new QueueBasedOnArray(4);
		q.push(5);
		q.pop();
		q.push(7);
		q.push(3);
		q.push(12);
		q.push(15);
		assertEquals("", 4, q.length());
	}

	@Test(expected = RuntimeException.class)
	public void checkQueueFullness() {
		QueueBasedOnArray q = new QueueBasedOnArray(4);
		q.push(5);
		q.pop();
		q.push(7);
		q.push(3);
		q.push(12);
		q.push(15);
		q.push(11);
	}
	
	@Test
	public void checkCircular() {
		QueueBasedOnArray q = new QueueBasedOnArray(4);
		q.push(5);
		q.push(7);
		q.push(3);
		q.pop();
		q.pop();
		q.push(12);
		q.push(15);
		q.push(11);
		assertEquals("", 4, q.length());
	}
}
