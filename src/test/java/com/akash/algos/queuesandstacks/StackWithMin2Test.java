package com.akash.algos.queuesandstacks;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class StackWithMin2Test {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testStackWithMin() {
		StackWithMin2 stack = new StackWithMin2();
		stack.push(3);
		stack.push(7);
		stack.push(9);
		Assert.assertEquals(3, stack.min());
	}
}
