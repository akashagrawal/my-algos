package com.akash.algos.queuesandstacks;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class MyQueueTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testMyQueue() {
		MyQueue q = new MyQueue();
		q.push(5);
		q.push(10);
		q.push(15);
		Assert.assertEquals(3, q.size());
		Assert.assertFalse(q.isEmpty());
		Assert.assertEquals(5, q.pop());
		Assert.assertEquals(10, q.peek());
		q.push(20);
		Assert.assertEquals(10, q.peek());
	}
}
