package com.akash.algos.queuesandstacks;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnimalShelterQueueTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testMyQueue() {
		AnimalShelterQueue q = new AnimalShelterQueue();
		Assert.assertNull(q.dequeueAny());
		Assert.assertNull(q.dequeueDog());
		Assert.assertNull(q.dequeueCat());
		q.enqueue(new Dog());
		Assert.assertNull(q.dequeueCat());
		Animal a = q.dequeueAny();
		Assert.assertTrue(a instanceof Dog);
	}
}
