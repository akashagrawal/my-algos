package com.akash.algos.queuesandstacks;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ThreeStackOneArrayTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testThreeStacks() {
		ThreeStacksOneArray tsoa = new ThreeStacksOneArray(3);
		tsoa.push(1, 15);
		tsoa.push(1, 10);
		tsoa.push(1, 12);
		Assert.assertEquals(3, tsoa.size(1));

		tsoa.push(2, 21);
		Assert.assertEquals(1, tsoa.size(2));
		Assert.assertEquals(21, tsoa.pop(2));
		Assert.assertEquals(0, tsoa.size(2));
		Assert.assertTrue(tsoa.isEmpty(2));
		
		Assert.assertEquals(0, tsoa.size(3));
		tsoa.push(3, 31);
		tsoa.push(3, 32);
		tsoa.push(3, 33);
		Assert.assertEquals(3, tsoa.size(3));
		Assert.assertFalse(tsoa.isEmpty(3));
		Assert.assertEquals(33, tsoa.pop(3));
	}
	
	@Test(expected = RuntimeException.class)
	public void testFullStackException() {
		ThreeStacksOneArray tsoa = new ThreeStacksOneArray(3);
		tsoa.push(1, 15);
		tsoa.push(1, 10);
		tsoa.push(1, 12);
		Assert.assertEquals(3, tsoa.size(1));
		tsoa.push(1, 18);
	}

	@Test(expected = RuntimeException.class)
	public void testPopStackException() {
		ThreeStacksOneArray tsoa = new ThreeStacksOneArray(3);
		tsoa.push(1, 15);
		tsoa.push(1, 10);
		tsoa.push(1, 12);
		Assert.assertEquals(3, tsoa.size(1));
		Assert.assertEquals(12, tsoa.pop(1));
		Assert.assertEquals(10, tsoa.pop(1));
		Assert.assertEquals(15, tsoa.pop(1));
		tsoa.pop(1);
	}

	@Test(expected = RuntimeException.class)
	public void testPeekStackException() {
		ThreeStacksOneArray tsoa = new ThreeStacksOneArray(3);
		Assert.assertEquals(0, tsoa.size(1));
		tsoa.peek(1);
	}
}
