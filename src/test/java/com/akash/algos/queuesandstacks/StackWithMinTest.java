package com.akash.algos.queuesandstacks;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class StackWithMinTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testStackWithMin() {
		StackWithMin stack = new StackWithMin();
		stack.push(3);
		stack.push(7);
		stack.push(9);
		Assert.assertEquals(3, stack.min());
		System.out.println(stack);
	}
}
