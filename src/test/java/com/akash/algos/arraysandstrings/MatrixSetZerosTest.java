package com.akash.algos.arraysandstrings;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class MatrixSetZerosTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void matrixSetZerosTest4x4() {
		int[][] matrix = {{1, 1, 1, 1},
				{1, 0, 1, 1},
				{1, 1, 1, 0},
				{0, 1, 1, 1}
				};
		
		MatrixSetZeros.printMatrix(matrix);
		MatrixSetZeros.setZeros(matrix);
		MatrixSetZeros.printMatrix(matrix);
		Assert.assertEquals(1, matrix[0][2]);
		Assert.assertEquals(0, matrix[2][2]);
	}
}
