package com.akash.algos.arraysandstrings;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.akash.algos.arraysandstrings.StringCompression;

public class StringCompressionTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void checkStringCompression() {
		Assert.assertEquals("a2b1c5d3", StringCompression.compressString("aabcccccddd"));
		Assert.assertEquals("a11", StringCompression.compressString("aaaaaaaaaaa"));
		Assert.assertEquals("abcdefghijk", StringCompression.compressString("abcdefghijk"));
	}
}
