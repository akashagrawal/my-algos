package com.akash.algos.arraysandstrings;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class RotateMatrix90Test {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void rotateMatrixTest5x5() {
		int[][] matrix = {{1, 2, 3, 4, 5},
				{6, 7, 8, 9, 10},
				{11, 12, 13, 14, 15},
				{16, 17, 18, 19, 20},
				{21, 22, 23, 24, 25}
				};
		
		RotateMatrix90.printMatrix(matrix);
		RotateMatrix90.rotateMatrix(matrix, matrix.length);
		RotateMatrix90.printMatrix(matrix);
		Assert.assertEquals(19, matrix[3][1]);
	}

	@Test
	public void rotateMatrixTest6x6() {
		int[][] matrix = {{1, 2, 3, 4, 5, 6},
				{7, 8, 9, 10, 11, 12},
				{13, 14, 15, 16, 17, 18},
				{19, 20, 21, 22, 23, 24},
				{25, 26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35, 36}
				};
		
		RotateMatrix90.printMatrix(matrix);
		RotateMatrix90.rotateMatrix(matrix, matrix.length);
		RotateMatrix90.printMatrix(matrix);
		Assert.assertEquals(28, matrix[3][1]);
	}
}
