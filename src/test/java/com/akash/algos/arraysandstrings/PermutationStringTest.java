package com.akash.algos.arraysandstrings;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.akash.algos.arraysandstrings.PermutationStrings;

public class PermutationStringTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void checkStringsHaveSameCharacters() {
		Assert.assertTrue("", PermutationStrings.arePermutationStrings("dog", "god"));
		Assert.assertFalse("", PermutationStrings.arePermutationStrings("dogs", "god"));
		Assert.assertTrue("", PermutationStrings.arePermutationStrings("akash is a bad boy", "a bad boy is akash"));
	}
}
