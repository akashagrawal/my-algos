package com.akash.algos.arraysandstrings;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.akash.algos.arraysandstrings.UniqueCharString;

public class UniqueCharStringTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void checkStringCharUniqueness() {
		boolean result = UniqueCharString.isUniqueCharInString("abcdefghijklmnopqrstuvwxyz");
		Assert.assertTrue("", result);
		
		result = UniqueCharString.isUniqueCharInString("abcdefghijklmnopqrstuvwxyzA");
		Assert.assertTrue("", result);

		result = UniqueCharString.isUniqueCharInString("abcdefghijklmnopqrstuvwxyzAa");
		Assert.assertFalse("", result);
	}
}
