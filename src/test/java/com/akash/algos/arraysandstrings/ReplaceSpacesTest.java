package com.akash.algos.arraysandstrings;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.akash.algos.arraysandstrings.ReplaceSpaces;

public class ReplaceSpacesTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void replaceSpacesWithArrayPassed() {
		// Requirement is that the array with enough spaces is already provided and this be
		// done in place
		char[] str = new char[256];
		String s = "akash is a bad boy.";
		s.getChars(0, s.length(), str, 0);
		String result = ReplaceSpaces.replaceSpacesWithArrayPassed(str, s.length());
		Assert.assertEquals("akash%20is%20a%20bad%20boy.", result);
	}

	@Test
	public void replaceSpacesWithStringPassed() {
		String s = "akash is a bad boy.";
		String result = ReplaceSpaces.replaceSpacesWithStringPassed(s);
		Assert.assertEquals("akash%20is%20a%20bad%20boy.", result);
	}
}
