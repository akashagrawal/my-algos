package com.akash.algos.treesandgraphs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class InOrderSuccessorTest {
	
	@BeforeClass
	public static void setUp() {
	}

	@Test
	public void testCreateMinimalBST1() {
		BNodeWithParent root = new BNodeWithParent(50);
		
		// 1st level
		BNodeWithParent left = new BNodeWithParent(30);
		BNodeWithParent right = new BNodeWithParent(70);
		root.left = left;
		root.right = right;
		left.parent = root;
		right.parent = root;

		// 2nd level
		BNodeWithParent left21 = new BNodeWithParent(20);
		BNodeWithParent right21 = new BNodeWithParent(40);
		left.left = left21;
		left.right = right21;
		left21.parent = left;
		right21.parent = left;

		BNodeWithParent left22 = new BNodeWithParent(60);
		BNodeWithParent right22 = new BNodeWithParent(80);
		right.left = left21;
		right.right = right21;
		left22.parent = right;
		right22.parent = right;
		
		// 3rd level
		BNodeWithParent right321 = new BNodeWithParent(45);
		right21.right = right321;
		right321.parent = right21;

		Assert.assertEquals(right321, InOrderSuccessor.inOrderSucc(right21));
		Assert.assertEquals(root, InOrderSuccessor.inOrderSucc(right321));
	}
}