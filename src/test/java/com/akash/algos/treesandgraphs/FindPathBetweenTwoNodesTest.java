package com.akash.algos.treesandgraphs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class FindPathBetweenTwoNodesTest {
	
	@BeforeClass
	public static void setUp() {
	}

	@Test
	public void testPathBetweenNodes1() {
		ALGraph g1 = createGraph1();
		System.out.println(g1.toString());
		
		// uses BFS
		boolean isPathExist = FindPathBetweenTwoNodes.searchBFS(g1, g1.getVertex(2), g1.getVertex(5));
		Assert.assertTrue(isPathExist);
		System.out.println();
	}

	@Test
	public void testPathBetweenNodes2() {
		ALGraph g1 = createGraph1();
		System.out.println(g1.toString());
		
		// uses DFS
		boolean isPathExist = FindPathBetweenTwoNodes.searchDFS(g1, g1.getVertex(0), g1.getVertex(6));
		Assert.assertTrue(isPathExist);
		
		// uses DFS
		isPathExist = FindPathBetweenTwoNodes.searchDFS(g1, g1.getVertex(5), g1.getVertex(7));
		Assert.assertTrue(isPathExist);
		System.out.println();
	}

	public ALGraph createGraph1() {
		ALGraph graph = new ALGraph(8);
		graph.setVertexName(0, "Malibu");
		graph.setVertexName(1, "Santa Barbara");
		graph.setVertexName(2, "Los Angeles");
		graph.setVertexName(3, "San Diego");
		graph.setVertexName(4, "Riverside");
		graph.setVertexName(5, "Barstow");
		graph.setVertexName(6, "Palm Springs");
		graph.setVertexName(7, "El Cajon");
		
		graph.addUndirectedEdge(0, 1, 45);
		graph.addUndirectedEdge(0, 2, 20);
		
		graph.addUndirectedEdge(1, 2, 30);
		graph.addUndirectedEdge(1, 5, 45);
		
		graph.addUndirectedEdge(2, 3, 100);
		graph.addUndirectedEdge(2, 4, 25);
		
		graph.addUndirectedEdge(3, 4, 90);
		graph.addUndirectedEdge(3, 7, 15);
		
		graph.addUndirectedEdge(4, 5, 75);
		graph.addUndirectedEdge(4, 6, 75);

		return graph;
	}
}