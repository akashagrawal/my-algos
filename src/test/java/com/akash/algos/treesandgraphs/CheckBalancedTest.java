package com.akash.algos.treesandgraphs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CheckBalancedTest {
	
	private static BinarySearchTree bst = null;
	
	@BeforeClass
	public static void setUp() {
	}

	@Test
	public void testTreeBalanced1() {
		int[] nodes = {5, 3, 7, 4, 6, 8, 2};
		bst = TreeHelper.buildTree(nodes);
		
		// inefficient algo
		Assert.assertTrue(CheckBalancedTree.isBalanced(bst.getRoot()));

		// efficient algo
		Assert.assertTrue(CheckBalancedTree.isBalancedEfficient(bst.getRoot()));
	}

	@Test
	public void testTreeBalanced2() {
		// inefficient algo
		Assert.assertTrue(CheckBalancedTree.isBalanced(null));

		BinaryNode node = new BinaryNode(5);
		Assert.assertTrue(CheckBalancedTree.isBalanced(node));

		BinaryNode node1 = new BinaryNode(5);
		BinaryNode node2 = new BinaryNode(7);
		node1.right = node2;
		Assert.assertTrue(CheckBalancedTree.isBalanced(node1));

		// efficient algo
		Assert.assertTrue(CheckBalancedTree.isBalancedEfficient(null));

		Assert.assertTrue(CheckBalancedTree.isBalancedEfficient(node));

		Assert.assertTrue(CheckBalancedTree.isBalancedEfficient(node1));
	}

	@Test
	public void testTreeBalanced3() {
		// inefficient algo
		int[] nodes = {10, 12, 9, 8};
		bst = TreeHelper.buildTree(nodes);
		Assert.assertTrue(CheckBalancedTree.isBalanced(bst.getRoot()));
		
		int[] nodes2 = {10, 12, 9, 8, 7};
		BinarySearchTree bst2 = TreeHelper.buildTree(nodes2);
		Assert.assertFalse(CheckBalancedTree.isBalanced(bst2.getRoot()));

		// efficient algo
		Assert.assertTrue(CheckBalancedTree.isBalancedEfficient(bst.getRoot()));

		Assert.assertFalse(CheckBalancedTree.isBalancedEfficient(bst2.getRoot()));
	}
}