package com.akash.algos.treesandgraphs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CreateMinimalBSTTest {
	
	@BeforeClass
	public static void setUp() {
	}

	@Test
	public void testCreateMinimalBST1() {
		int[] array = {2, 3, 4, 5, 6, 7, 8};
		BinaryNode node = CreateMinimalBST.createMinimalBST(array);
		Assert.assertEquals(3, BinarySearchTree.height(node));
	}

	@Test
	public void testCreateMinimalBST2() {
		int[] array = {2, 3, 4, 5, 6, 7, 8, 9};
		BinaryNode node = CreateMinimalBST.createMinimalBST(array);
		Assert.assertEquals(4, BinarySearchTree.height(node));
	}
}