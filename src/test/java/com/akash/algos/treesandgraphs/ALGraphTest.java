package com.akash.algos.treesandgraphs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ALGraphTest {
	
	@BeforeClass
	public static void setUp() {
	}

	@Test
	public void testDfsRecursive() {
		ALGraph g1 = createGraph1();
		System.out.println(g1.toString());
		
		g1.dfsRecursiveTraversal(0);
		g1.dfsRecursiveTraversal(1);
		g1.dfsRecursiveTraversal(2);
		g1.dfsRecursiveTraversal(3);
		g1.dfsRecursiveTraversal(4);
		g1.dfsRecursiveTraversal(5);
		System.out.println();
	}

	@Test
	public void testDfsIterative() {
		ALGraph g1 = createGraph1();
		// System.out.println(g1.toString());
		
		g1.dfsIterativeTraversal(0);
		g1.dfsIterativeTraversal(1);
		g1.dfsIterativeTraversal(2);
		g1.dfsIterativeTraversal(3);
		g1.dfsIterativeTraversal(4);
		g1.dfsIterativeTraversal(5);
		System.out.println();
	}
	
	@Test
	public void testBfsIterative() {
		ALGraph g1 = createGraph1();
		// System.out.println(g1.toString());
		
		g1.bfsIterativeTraversal(0);
		g1.bfsIterativeTraversal(1);
		g1.bfsIterativeTraversal(2);
		g1.bfsIterativeTraversal(3);
		g1.bfsIterativeTraversal(4);
		g1.bfsIterativeTraversal(5);
		System.out.println();
	}

	@Test
	public void testGraphWithABST() {
		// TODO build a BST and do dfs, and bfs
	}

	public ALGraph createGraph1() {
		ALGraph graph = new ALGraph(6);
		graph.setVertexName(0, "Index.htm");
		graph.setVertexName(1, "About.htm");
		graph.setVertexName(2, "Privacy.htm");
		graph.setVertexName(3, "Contact.aspx");
		graph.setVertexName(4, "Products.aspx");
		graph.setVertexName(5, "People.aspx");
		
		graph.addDirectedEdge(0, 4, 0);
		graph.addDirectedEdge(0, 3, 0);
		graph.addDirectedEdge(0, 1, 0);
		
		graph.addDirectedEdge(1, 2, 0);
		graph.addDirectedEdge(1, 4, 0);
		graph.addDirectedEdge(1, 5, 0);

		graph.addDirectedEdge(2, 0, 0);
		graph.addDirectedEdge(2, 1, 0);
		
		graph.addDirectedEdge(4, 5, 0);
		graph.addDirectedEdge(4, 0, 0);

		graph.addDirectedEdge(5, 2, 0);
		return graph;
	}
}