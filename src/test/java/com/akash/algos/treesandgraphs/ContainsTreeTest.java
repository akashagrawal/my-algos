package com.akash.algos.treesandgraphs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ContainsTreeTest {
	
	@BeforeClass
	public static void setUp() {
	}

	@Test
	public void testContainsTree() {
		int[] array = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
		BinaryNode largeTree = CreateMinimalBST.createMinimalBST(array);
		
		int[] array2 = {2, 3, 4};
		BinaryNode smallTree = CreateMinimalBST.createMinimalBST(array2);
		
		Assert.assertTrue(ContainsTree.containsTree(largeTree, smallTree));
	}
}