package com.akash.algos.treesandgraphs;

import java.util.ArrayList;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CreateListsFromTreeTest {
	
	@BeforeClass
	public static void setUp() {
	}

	@Test
	public void testCreateMinimalBST1() {
		int[] array = {2, 3, 4, 5, 6, 7, 8};
		BinaryNode node = CreateMinimalBST.createMinimalBST(array);
		
		ArrayList<LinkedList<BinaryNode>> lists = CreateListsFromTree.createLinkedListsFromTree(node);
		Assert.assertEquals(3, lists.size());
		Assert.assertEquals(1, lists.get(0).size());
		Assert.assertEquals(2, lists.get(1).size());
		Assert.assertEquals(4, lists.get(2).size());
		System.out.println(lists);
	}

	@Test
	public void testCreateMinimalBST2() {
		int[] array = {2, 3, 4, 5, 6, 7, 8};
		BinarySearchTree bst = TreeHelper.buildTree(array);
		
		ArrayList<LinkedList<BinaryNode>> lists = CreateListsFromTree.createLinkedListsFromTree(bst.getRoot());
		Assert.assertEquals(7, lists.size());
		Assert.assertEquals(1, lists.get(0).size());
		Assert.assertEquals(1, lists.get(1).size());
		Assert.assertEquals(1, lists.get(6).size());
		System.out.println(lists);
	}
}