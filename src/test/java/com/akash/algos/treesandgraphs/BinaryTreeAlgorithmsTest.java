package com.akash.algos.treesandgraphs;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class BinaryTreeAlgorithmsTest {
	
	private static BinarySearchTree bst = null;
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void testInorder() {
		int[] nodes = {5, 3, 7, 4, 6, 8, 2};
		bst = TreeHelper.buildTree(nodes);

		System.out.print("Inorder => ");
		List<Integer> list = new ArrayList<Integer>();
		BinaryTreeAlgorithms.inorderTraversal(bst.getRoot(), list);
		System.out.println("");
		Assert.assertEquals(2, (int)list.get(0));
		Assert.assertEquals(8, (int)list.get(6));
	}

	@Test
	public void testPreorder() {
		int[] nodes = {5, 3, 7, 4, 6, 8, 2};
		bst = TreeHelper.buildTree(nodes);

		System.out.print("Preorder => ");
		List<Integer> list = new ArrayList<Integer>();
		BinaryTreeAlgorithms.preorderTraversal(bst.getRoot(), list);
		System.out.println("");
		Assert.assertEquals(5, (int)list.get(0));
		Assert.assertEquals(8, (int)list.get(6));
	}

	@Test
	public void testPostorder() {
		int[] nodes = {5, 3, 7, 4, 6, 8, 2};
		bst = TreeHelper.buildTree(nodes);

		System.out.print("Postorder => ");
		List<Integer> list = new ArrayList<Integer>();
		BinaryTreeAlgorithms.postorderTraversal(bst.getRoot(), list);
		System.out.println("");
		Assert.assertEquals(2, (int)list.get(0));
		Assert.assertEquals(5, (int)list.get(6));
	}

	@Test
	public void testLevelorder() {
		int[] nodes = {5, 3, 7, 4, 6, 8, 2};
		bst = TreeHelper.buildTree(nodes);

		System.out.print("Levelorder => ");
		List<Integer> list = new ArrayList<Integer>();
		BinaryTreeAlgorithms.levelorderTraversal(bst.getRoot(), list);
		System.out.println("");
		Assert.assertEquals(3, (int)list.get(1));
		Assert.assertEquals(2, (int)list.get(3));
	}

	@Test
	public void testHasPathSum() {
		int[] nodes = {5, 3, 7, 4, 6, 8};
		bst = TreeHelper.buildTree(nodes);
		Assert.assertEquals(false, BinaryTreeAlgorithms.hasPathSum(bst.getRoot(), 13));
		Assert.assertEquals(false, BinaryTreeAlgorithms.hasPathSum(bst.getRoot(), 10));
		Assert.assertEquals(true, BinaryTreeAlgorithms.hasPathSum(bst.getRoot(), 12));
		Assert.assertEquals(true, BinaryTreeAlgorithms.hasPathSum(bst.getRoot(), 18));
		Assert.assertEquals(true, BinaryTreeAlgorithms.hasPathSum(bst.getRoot(), 20));
		Assert.assertEquals(false, BinaryTreeAlgorithms.hasPathSum(bst.getRoot(), 8));
	}

	@Test
	public void testPrintPaths() {
		int[] nodes = {5, 3, 7, 4, 6, 8, 2};
		bst = TreeHelper.buildTree(nodes);

		BinaryTreeAlgorithms.printPaths(bst.getRoot());
	}

	@Test
	public void testPrintPathsMatchesSum() {
		int[] nodes = {5, 3, 7, 4, 6, 8};
		bst = TreeHelper.buildTree(nodes);
		BinaryTreeAlgorithms.printPathsMatchesSum(bst.getRoot(), 12);
		BinaryTreeAlgorithms.printPathsMatchesSum(bst.getRoot(), 20);
	}

	@Test
	public void testMirror() {
		int[] nodes = {4, 2, 5, 1, 3};
		bst = TreeHelper.buildTree(nodes);
		List<Integer> q = new ArrayList<Integer>();
		BinaryTreeAlgorithms.inorderTraversal(bst.getRoot(), q);
		Assert.assertEquals(1, (int)q.get(0));
		System.out.println(q);
		
		BinaryTreeAlgorithms.mirror(bst.getRoot());
		q.clear();
		BinaryTreeAlgorithms.inorderTraversal(bst.getRoot(), q);
		Assert.assertEquals(5, (int)q.get(0));
		System.out.println(q);
	}

	@Test
	public void testDoubleTree() {
		int[] nodes = {2, 1, 3};
		bst = TreeHelper.buildTree(nodes);
		BinaryTreeAlgorithms.doubleTree(bst.getRoot());
		List<Integer> list = new ArrayList<Integer>();
		BinaryTreeAlgorithms.inorderTraversal(bst.getRoot(), list);
		Assert.assertEquals(6, list.size());
		Assert.assertEquals(1, (int)list.get(0));
		Assert.assertEquals(3, (int)list.get(5));
	}

	@Test
	public void testSameTree() {
		int[] nodes1 = {2, 1, 3};
		int[] nodes2 = {5, 3, 7, 4, 6, 8, 2};
		BinarySearchTree bst1 = TreeHelper.buildTree(nodes1);
		BinarySearchTree bst2 = TreeHelper.buildTree(nodes2);
		Assert.assertFalse(BinaryTreeAlgorithms.sameTree(bst1.getRoot(), bst2.getRoot()));
		
		BinarySearchTree bst3 = TreeHelper.buildTree(nodes2);
		Assert.assertTrue(BinaryTreeAlgorithms.sameTree(bst3.getRoot(), bst2.getRoot()));
	}
	
	@Test
	public void testIsBinarySearchTree() {
		BinaryNode node1 = new BinaryNode(5);
		BinaryNode node2 = new BinaryNode(2);
		BinaryNode node3 = new BinaryNode(7);
		BinaryNode node4 = new BinaryNode(6);
		BinaryNode node5 = new BinaryNode(1);
		node1.right = node3;
		node1.left = node2;
		node2.right = node4;
		node2.left = node5;
		Assert.assertFalse(BinaryTreeAlgorithms.isBinarySearchTree(node1));

		int[] nodes2 = {5, 3, 7, 4, 6, 8, 2};
		BinarySearchTree bst2 = TreeHelper.buildTree(nodes2);
		Assert.assertTrue(BinaryTreeAlgorithms.isBinarySearchTree(bst2));
	}

	@Test
	public void testIsBinarySearchTree2() {
		BinaryNode node1 = new BinaryNode(5);
		BinaryNode node2 = new BinaryNode(2);
		BinaryNode node3 = new BinaryNode(7);
		BinaryNode node4 = new BinaryNode(6);
		BinaryNode node5 = new BinaryNode(1);
		node1.right = node3;
		node1.left = node2;
		node2.right = node4;
		node2.left = node5;
		Assert.assertFalse(BinaryTreeAlgorithms.isBinarySearchTree2(node1, Integer.MIN_VALUE, Integer.MAX_VALUE));

		int[] nodes2 = {5, 3, 7, 4, 6, 8, 2};
		BinarySearchTree bst2 = TreeHelper.buildTree(nodes2);
		Assert.assertTrue(BinaryTreeAlgorithms.isBinarySearchTree2(bst2));
	}
}