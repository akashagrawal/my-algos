package com.akash.algos.treesandgraphs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AMGraphTest {
	
	@BeforeClass
	public static void setUp() {
	}

	@Test
	public void testDfsRecursive() {
		AMGraph g1 = createGraph1();
		System.out.println(g1.toString());
		
		g1.dfsRecursiveTraversal(0);
		g1.dfsRecursiveTraversal(1);
		g1.dfsRecursiveTraversal(2);
		g1.dfsRecursiveTraversal(3);
		g1.dfsRecursiveTraversal(4);
		g1.dfsRecursiveTraversal(5);
		System.out.println();
	}

	@Test
	public void testDfsIterative() {
		AMGraph g1 = createGraph1();
		// System.out.println(g1.toString());
		
		g1.dfsIterativeTraversal(0);
		g1.dfsIterativeTraversal(1);
		g1.dfsIterativeTraversal(2);
		g1.dfsIterativeTraversal(3);
		g1.dfsIterativeTraversal(4);
		g1.dfsIterativeTraversal(5);
		System.out.println();
	}
	
	@Test
	public void testGraphWithABST() {
		// TODO build a BST and do dfs, and bfs
	}

	public AMGraph createGraph1() {
		AMGraph graph = new AMGraph(6);
		graph.setVertexName(0, "Index.htm");
		graph.setVertexName(1, "About.htm");
		graph.setVertexName(2, "Privacy.htm");
		graph.setVertexName(3, "Contact.aspx");
		graph.setVertexName(4, "Products.aspx");
		graph.setVertexName(5, "People.aspx");
		
		graph.addDirectedEdge(0, 4, 1);
		graph.addDirectedEdge(0, 3, 1);
		graph.addDirectedEdge(0, 1, 1);
		
		graph.addDirectedEdge(1, 2, 1);
		graph.addDirectedEdge(1, 4, 1);
		graph.addDirectedEdge(1, 5, 1);

		graph.addDirectedEdge(2, 0, 1);
		graph.addDirectedEdge(2, 1, 1);
		
		graph.addDirectedEdge(4, 5, 1);
		graph.addDirectedEdge(4, 0, 1);

		graph.addDirectedEdge(5, 2, 1);
		return graph;
	}
}