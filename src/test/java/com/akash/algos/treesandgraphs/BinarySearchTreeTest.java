package com.akash.algos.treesandgraphs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class BinarySearchTreeTest {
	
	private static BinarySearchTree bst = null;
	
	@BeforeClass
	public static void setUp() {
		int[] nodes = {5, 3, 7, 4, 6, 8, 2};
		bst = TreeHelper.buildTree(nodes);
	}
	
	@Test
	public void testBST() {
		Assert.assertEquals(6, bst.findIterative(6).value);
		Assert.assertEquals(4, bst.findRecursive(bst.getRoot(), 4).value);
	}

	@Test
	public void testTreeSize() {
		Assert.assertEquals(7, bst.size());
	}

	@Test
	public void testTreeMaxDepth1() {
		Assert.assertEquals(3, bst.maxDepth());
		
		BinarySearchTree nullRootTree = new BinarySearchTree(null);
		Assert.assertEquals(0, nullRootTree.maxDepth());

		BinaryNode node = new BinaryNode(5);
		BinarySearchTree oneNodeTree = new BinarySearchTree(node);
		Assert.assertEquals(1, oneNodeTree.maxDepth());
	}

	@Test
	public void testTreeMaxDepth2() {
		BinaryNode node1 = new BinaryNode(5);
		BinaryNode node2 = new BinaryNode(7);
		node1.right = node2;
		BinarySearchTree twoNodeTree = new BinarySearchTree(node1);
		Assert.assertEquals(2, twoNodeTree.maxDepth());
		
		node1.value = 5;
		node2.value = 3;
		node1.right = null;
		node1.left = node2;
		twoNodeTree.setRoot(node1);
		Assert.assertEquals(2, twoNodeTree.maxDepth());
		
		node1.value = 5;
		node2.value = 3;
		BinaryNode node3 = new BinaryNode(7);
		node1.right = node3;
		node1.left = node2;
		twoNodeTree.setRoot(node1);
		Assert.assertEquals(2, twoNodeTree.maxDepth());
	}

	@Test
	public void testTreeMaxDepth3() {
		BinaryNode node1 = new BinaryNode(50);
		BinaryNode node2 = new BinaryNode(30);
		BinaryNode node3 = new BinaryNode(90);
		BinaryNode node4 = new BinaryNode(40);
		BinaryNode node5 = new BinaryNode(80);
		BinaryNode node6 = new BinaryNode(85);
		BinaryNode node7 = new BinaryNode(83);
		node1.right = node3;
		node1.left = node2;
		node2.right = node4;
		node3.left = node5;
		node5.right = node6;
		node6.left = node7;
		BinaryTreeAlgorithms.inorderTraversal(node1);
		BinarySearchTree multiNodeTree = new BinarySearchTree(node1);
		Assert.assertEquals(5, multiNodeTree.maxDepth());
	}

	@Test
	public void testTreeMinimum() {
		Assert.assertEquals(2, bst.minValue());
	}

	@Test
	public void testTreeMaximum() {
		Assert.assertEquals(8, bst.maxValue());
	}

}
