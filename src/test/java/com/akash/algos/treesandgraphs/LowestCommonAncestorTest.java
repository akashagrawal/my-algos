package com.akash.algos.treesandgraphs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class LowestCommonAncestorTest {
	private static BinarySearchTree bst;
	
	@BeforeClass
	public static void setUp() {
		int[] nodes = {5, 3, 7, 4, 6, 8, 2};
		bst = TreeHelper.buildTree(nodes);
	}

	@Test
	public void testCommonAncestor1() {
		BinaryNode n1 = bst.getRoot().left.right;
		BinaryNode n2 = bst.getRoot().left.left;
		
		Assert.assertEquals(bst.getRoot().left, LowestCommonAncestor.lcaBSTRecursive(bst.getRoot(), n1, n2));
		
		BinaryNode n3 = bst.getRoot().right;
		Assert.assertEquals(bst.getRoot(), LowestCommonAncestor.lcaBSTRecursive(bst.getRoot(), n1, n3));
	}

	@Test
	public void testCommonAncestor2() {
		BinaryNode n1 = bst.getRoot().left.right;
		BinaryNode n2 = bst.getRoot().left.left;
		
		Assert.assertEquals(bst.getRoot().left, LowestCommonAncestor.lcaBSTIterative(bst.getRoot(), n1, n2));
		
		BinaryNode n3 = bst.getRoot().right;
		Assert.assertEquals(bst.getRoot(), LowestCommonAncestor.lcaBSTIterative(bst.getRoot(), n1, n3));
	}

	@Test
	public void testCommonAncestor3() {
		BinaryNode n1 = bst.getRoot().left.right;
		BinaryNode n2 = bst.getRoot().left.left;
		
		Assert.assertEquals(bst.getRoot().left, LowestCommonAncestor.lcaBinaryTree(bst.getRoot(), n1, n2));
		
		BinaryNode n3 = bst.getRoot().right;
		Assert.assertEquals(bst.getRoot(), LowestCommonAncestor.lcaBinaryTree(bst.getRoot(), n1, n3));
	}
}