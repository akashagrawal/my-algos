package com.akash.algos.linkedlists;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeListTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void checkEvenListPalindrome() {
		// Create a singly linked list
		int[] nodeValues = {1, 2, 3, 4, 4, 3, 2, 1};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		
		boolean result = PalindromeList.isPalindrome(head);
		Assert.assertEquals(true, result);
	}

	@Test
	public void checkOddListPalindrome() {
		// Create a singly linked list
		int[] nodeValues = {1, 2, 3, 4, 5, 4, 3, 2, 1};
		Node head = ListHelper.createSinglyLinkedList(nodeValues.length, nodeValues);
		ListHelper.printLinkedList(head);
		
		boolean result = PalindromeList.isPalindrome(head);
		Assert.assertEquals(true, result);
	}

	@Test
	public void checkEvenListPalindrome2() {
		// Create a singly linked list
		int[] nodeValues = {1, 2, 3, 4, 4, 3, 1, 1};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		
		boolean result = PalindromeList.isPalindrome(head);
		Assert.assertEquals(false, result);
	}

	@Test
	public void checkOddListPalindrome2() {
		// Create a singly linked list
		int[] nodeValues = {1, 2, 3, 4, 5, 4, 3, 2, 2};
		Node head = ListHelper.createSinglyLinkedList(nodeValues.length, nodeValues);
		ListHelper.printLinkedList(head);
		
		boolean result = PalindromeList.isPalindrome(head);
		Assert.assertEquals(false, result);
	}
}
