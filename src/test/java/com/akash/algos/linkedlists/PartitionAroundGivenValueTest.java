package com.akash.algos.linkedlists;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class PartitionAroundGivenValueTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void deleteNode() {
		// Create a singly linked list
		int[] nodeValues = {20, 30, 20, 40, 50, 30, 40, 40};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		
		Node result = PartitionAroundGivenValue.partition(head, 40);
		ListHelper.printLinkedList(result);
		Assert.assertEquals(30, result.next.next.next.value);
	}
}
