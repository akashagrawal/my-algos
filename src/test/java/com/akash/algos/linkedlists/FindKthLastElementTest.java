package com.akash.algos.linkedlists;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.akash.algos.linkedlists.FindKthLastElement.IntWrapper;

public class FindKthLastElementTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void findKthFromLast() {
		// Create a singly linked list
		int[] nodeValues = {20, 30, 20, 40, 50, 30, 40, 40};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		// Find 4th from the last
		Node result = FindKthLastElement.findKthLastElement(head, 4);
		Assert.assertEquals(50, result.value);
		// Find 0th from the last
		result = FindKthLastElement.findKthLastElement(head, 0);
		Assert.assertEquals(null, result);
		// Find -1th from the last
		result = FindKthLastElement.findKthLastElement(head, -1);
		Assert.assertEquals(null, result);
		
		// One node list
		nodeValues = new int[1];
		nodeValues[0] = 20;
		head = ListHelper.createSinglyLinkedList(1, nodeValues);
		// Find 4th from the last
		result = FindKthLastElement.findKthLastElement(head, 4);
		Assert.assertEquals(null, result);
		// Find 1st from the last
		result = FindKthLastElement.findKthLastElement(head, 1);
		Assert.assertEquals(20, result.value);
		
		// Two nodes list
		nodeValues = new int[2];
		nodeValues[0] = 20; nodeValues[1] = 30;
		head = ListHelper.createSinglyLinkedList(2, nodeValues);
		// Find 1st from the last
		result = FindKthLastElement.findKthLastElement(head, 1);
		Assert.assertEquals(30, result.value);
		// Find 2nd from the last
		result = FindKthLastElement.findKthLastElement(head, 2);
		Assert.assertEquals(20, result.value);
		// Find 3rd from the last
		result = FindKthLastElement.findKthLastElement(head, 3);
		Assert.assertEquals(null, result);

		// NULL node list
		result = FindKthLastElement.findKthLastElement(null, 1);
		Assert.assertEquals(null, result);
	}

	@Test
	public void findKthFromLastRecursive() {
		// Create a singly linked list
		int[] nodeValues = {20, 30, 20, 40, 50, 30, 40, 40};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		
		// Find 4th from the last
		Node result = FindKthLastElement.findKthLastRecursive(head, 4, new IntWrapper());
		Assert.assertEquals(50, result.value);
		
		// Find 0th from the last
		result = FindKthLastElement.findKthLastRecursive(head, 0, new IntWrapper());
		Assert.assertEquals(null, result);
		
		// Find -1th from the last
		result = FindKthLastElement.findKthLastRecursive(head, -1, new IntWrapper());
		Assert.assertEquals(null, result);
	}

	@Test
	public void findKthFromLastRecursiveUsingStatic() {
		// Create a singly linked list
		int[] nodeValues = {20, 30, 20, 40, 50, 30, 40, 40};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		
		// Find 4th from the last
		Node result = FindKthLastElement.findKthLastRecursiveUsingStaticInt(head, 4);
		Assert.assertEquals(50, result.value);
		
		// Find 0th from the last
		result = FindKthLastElement.findKthLastRecursiveUsingStaticInt(head, 0);
		Assert.assertEquals(null, result);
		
		// Find -1th from the last
		result = FindKthLastElement.findKthLastRecursiveUsingStaticInt(head, -1);
		Assert.assertEquals(null, result);
	}
}
