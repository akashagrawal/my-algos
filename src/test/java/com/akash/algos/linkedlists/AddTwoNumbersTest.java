package com.akash.algos.linkedlists;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AddTwoNumbersTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void addTwoNumbers() {
		// Create singly linked lists
		int[] n1 = {7, 1, 6};
		Node head1 = ListHelper.createSinglyLinkedList(3, n1);
		ListHelper.printLinkedList(head1);
		int[] n2 = {5, 9, 2};
		Node head2 = ListHelper.createSinglyLinkedList(3, n2);
		ListHelper.printLinkedList(head2);
		
		Node result = AddTwoNumbers.addTwoNumbers(head1, head2);
		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.value);
		ListHelper.printLinkedList(result);
	}

	@Test
	public void addTwoNumbers2() {
		// Create singly linked lists
		int[] n1 = {7, 1, 6, 9, 9};
		Node head1 = ListHelper.createSinglyLinkedList(n1.length, n1);
		ListHelper.printLinkedList(head1);
		int[] n2 = {5, 9, 4};
		Node head2 = ListHelper.createSinglyLinkedList(n2.length, n2);
		ListHelper.printLinkedList(head2);
		
		Node result = AddTwoNumbers.addTwoNumbers(head1, head2);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.next.next.next.next.next.value);
		ListHelper.printLinkedList(result);
	}

	@Test
	public void addTwoNumbersRecursive1() {
		// Create singly linked lists
		int[] n1 = {7, 1, 6};
		Node head1 = ListHelper.createSinglyLinkedList(3, n1);
		ListHelper.printLinkedList(head1);
		int[] n2 = {5, 9, 2};
		Node head2 = ListHelper.createSinglyLinkedList(3, n2);
		ListHelper.printLinkedList(head2);
		
		Node result = AddTwoNumbers.addTwoNumbersRecursively(head1, head2, 0);
		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.value);
		ListHelper.printLinkedList(result);
	}

	@Test
	public void addTwoNumbersRecursive2() {
		// Create singly linked lists
		int[] n1 = {7, 1, 6, 9, 9};
		Node head1 = ListHelper.createSinglyLinkedList(n1.length, n1);
		ListHelper.printLinkedList(head1);
		int[] n2 = {5, 9, 4};
		Node head2 = ListHelper.createSinglyLinkedList(n2.length, n2);
		ListHelper.printLinkedList(head2);
		
		Node result = AddTwoNumbers.addTwoNumbersRecursively(head1, head2, 0);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.next.next.next.next.next.value);
		ListHelper.printLinkedList(result);
	}
}
