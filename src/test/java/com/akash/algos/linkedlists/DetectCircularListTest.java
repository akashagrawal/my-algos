package com.akash.algos.linkedlists;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DetectCircularListTest {
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void checkListNotCircular() {
		// Create a singly linked list
		int[] nodeValues = {20, 30, 20, 40, 50, 30, 40, 40};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		boolean result = DetectCircularList.isCircular(head);
		Assert.assertEquals(false, result);
	}

	@Test
	public void checkListCircular() {
		// Create a singly linked list
		int[] nodeValues = {20, 30, 20, 40, 50, 30, 40, 40};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		Node end = head;
		while(end.next != null) {
			end = end.next;
		}
		end.next = head;
		boolean result = DetectCircularList.isCircular(head);
		Assert.assertEquals(true, result);
	}
}
