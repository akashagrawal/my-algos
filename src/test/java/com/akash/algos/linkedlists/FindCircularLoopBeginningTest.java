package com.akash.algos.linkedlists;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class FindCircularLoopBeginningTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void findBeginning() {
		// Create a singly linked list
		int[] nodeValues = {20, 30, 20, 40, 50, 30, 40, 40};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		Node end = head;
		while(end.next != null) {
			end = end.next;
		}
		end.next = head.next.next.next;

		Node result = FindCircularLoopBeginning.findBeginning(head);
		Assert.assertEquals(40, result.value);
	}

	@Test
	public void findBeginning2() {
		// Create a singly linked list
		Node head = ListHelper.createSinglyLinkedList(80);
		ListHelper.printLinkedList(head);
		Node end = head;
		while(end.next != null) {
			end = end.next;
		}
		Node temp = head;
		for(int index=0;index < 20;index++) {
			temp = temp.next;
		}
		end.next = temp;

		Node result = FindCircularLoopBeginning.findBeginning(head);
		Assert.assertEquals(210, result.value);
	}
}
