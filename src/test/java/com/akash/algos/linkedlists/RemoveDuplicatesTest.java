package com.akash.algos.linkedlists;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class RemoveDuplicatesTest {
	
	@BeforeClass
	public static void setUp() {
	}
	
	@Test
	public void removeDuplicateNodes() {
		// Create a singly linked list
		int[] nodeValues = {20, 30, 20, 40, 50, 30, 40, 40};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		// remove duplicates
		RemoveDuplicates.removeDuplicateNodes(head);
		ListHelper.printLinkedList(head);
		Assert.assertEquals(4, ListHelper.getNumberOfNodes(head));
		
		// One node list
		nodeValues = new int[1];
		nodeValues[0] = 20;
		head = ListHelper.createSinglyLinkedList(1, nodeValues);
		RemoveDuplicates.removeDuplicateNodes(head);
		Assert.assertEquals(1, ListHelper.getNumberOfNodes(head));
		
		// Two nodes list
		nodeValues = new int[2];
		nodeValues[0] = 20; nodeValues[1] = 30;
		head = ListHelper.createSinglyLinkedList(2, nodeValues);
		RemoveDuplicates.removeDuplicateNodes(head);
		Assert.assertEquals(2, ListHelper.getNumberOfNodes(head));

		// Two nodes list
		nodeValues = new int[2];
		nodeValues[0] = 30; nodeValues[1] = 30;
		head = ListHelper.createSinglyLinkedList(2, nodeValues);
		RemoveDuplicates.removeDuplicateNodes(head);
		Assert.assertEquals(1, ListHelper.getNumberOfNodes(head));

		// NULL node list
		RemoveDuplicates.removeDuplicateNodes(null);
		Assert.assertEquals(0, ListHelper.getNumberOfNodes(null));
	}

	@Test
	public void removeDuplicateNodesNoExtraBuffer() {
		// Create a singly linked list
		int[] nodeValues = {20, 30, 20, 40, 50, 30, 40, 40};
		Node head = ListHelper.createSinglyLinkedList(8, nodeValues);
		ListHelper.printLinkedList(head);
		// remove duplicates
		RemoveDuplicates.removeDupNoExtraBuffer(head);
		ListHelper.printLinkedList(head);
		Assert.assertEquals(4, ListHelper.getNumberOfNodes(head));
		
		// One node list
		nodeValues = new int[1];
		nodeValues[0] = 20;
		head = ListHelper.createSinglyLinkedList(1, nodeValues);
		RemoveDuplicates.removeDupNoExtraBuffer(head);
		Assert.assertEquals(1, ListHelper.getNumberOfNodes(head));
		
		// Two nodes list
		nodeValues = new int[2];
		nodeValues[0] = 20; nodeValues[1] = 30;
		head = ListHelper.createSinglyLinkedList(2, nodeValues);
		RemoveDuplicates.removeDupNoExtraBuffer(head);
		Assert.assertEquals(2, ListHelper.getNumberOfNodes(head));

		// Two nodes list
		nodeValues = new int[2];
		nodeValues[0] = 30; nodeValues[1] = 30;
		head = ListHelper.createSinglyLinkedList(2, nodeValues);
		RemoveDuplicates.removeDupNoExtraBuffer(head);
		Assert.assertEquals(1, ListHelper.getNumberOfNodes(head));

		// NULL node list
		RemoveDuplicates.removeDupNoExtraBuffer(null);
		Assert.assertEquals(0, ListHelper.getNumberOfNodes(null));
	}
}
